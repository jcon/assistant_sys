package action.study;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import service.study.StudyPlanService;
import tools.LogUtil;
import tools.StringTools;
import entity.study.StudyPlan;
import entity.work.WorkPlan;
import action.baseAction;

public class StudyPlanAction extends baseAction<StudyPlan>{
	
	private StudyPlanService studyPlanService;

	@Override
	public String index() {
		if (StringTools.isNullOrBlank(getLoginAccount())) {//判断用户是否登录
			return "login";
		}
		LogUtil.out("获取工作笔记主页");
		return "success";
	}

	@Override
	public String getJsonList() {
		LogUtil.out("当前操作用户："+getLoginAccount());
		List<StudyPlan> list = studyPlanService.getList(getStartPage(),getPageSize(),getLoginAccount());
		LogUtil.out(getStartPage()+"*******"+getPageSize());
		int count = studyPlanService.getCount(getLoginAccount());
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("total", count);
		map.put("rows", list);
		setMap(map);
		LogUtil.out(getMap().toString());
		return "json";
	}

	@Override
	public String deleteById() {
		LogUtil.out("删除操作>>>>"+model.toString());
		boolean deleteById = studyPlanService.deleteById(model.getId(),getLoginAccount());
		return null;
	}

	@Override
	public String save() {
		LogUtil.out("新增操作>>>>"+model.toString());
		model.setCreateTime(new Date());
		LogUtil.out("现在时间"+new Date());
		model.setAccount(getLoginAccount());
		studyPlanService.save(model);
		return SUCCESS;
	}

	@Override
	public String find() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String update() {
		LogUtil.out("修改操作>>>"+model.toString());
		studyPlanService.update(model);
		return SUCCESS;
	}

	public void setStudyPlanService(StudyPlanService studyPlanService) {
		this.studyPlanService = studyPlanService;
	}

	
}
