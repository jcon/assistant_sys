package action.sys;

import java.util.Date;

import javax.servlet.http.HttpSession;

import service.sys.AccountService;
import tools.LogUtil;
import action.baseAction;
import entity.sys.Account;

public class AccountAction extends baseAction<Account>{
	private AccountService accountService;

	@Override
	public String index() {
		// 1.密码验证
		LogUtil.out("接收到的用户名为："+model.getAccount());
		Account account = accountService.findByAccount(model.getAccount());
		if (account == null) {
			LogUtil.out("账号错误！");
		}
		if (account.getAccount().equals(model.getAccount()) && account.getPassword().equals(model.getPassword())) {
			LogUtil.out("登录成功！");
			setSessionCon();
			return SUCCESS;
		}else {
			LogUtil.out("密码错误！");
		}
		// 2.设置session保持会话
		// 3.跳转到首页
		setSessionCon();
		return SUCCESS;
	}

	private void setSessionCon() {
		HttpSession session = getRequest().getSession();
		session.setAttribute("user", model.getAccount());
		session.setAttribute("isLogin", true);
	}
	
	@Override
	public String getJsonList() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String deleteById() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String save() {
		model.setCreateTime(new Date());
		LogUtil.out("保存注册用户"+model.toString());
		accountService.save(model);
		setSessionCon();//设置用户名到session中
		return SUCCESS;
	}

	@Override
	public String find() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String update() {
		// TODO Auto-generated method stub
		return null;
	}
	
	//退出登录
	public String loginOut(){
		HttpSession session = getRequest().getSession();
		session.removeAttribute("user");
		session.removeAttribute("isLogin");
		return SUCCESS;
	}

	public void setAccountService(AccountService accountService) {
		this.accountService = accountService;
	}

	
	
}
