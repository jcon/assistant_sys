package action.health;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import service.health.HealthSummarizeService;
import tools.LogUtil;
import tools.StringTools;
import action.baseAction;
import entity.health.HealthSummarize;

public class HealthSummarizeAction  extends baseAction<HealthSummarize>{
	
	private HealthSummarizeService healthSummarizeService;

	@Override
	public String index() {
		if (StringTools.isNullOrBlank(getLoginAccount())) {//判断用户是否登录
			return "login";
		}
		LogUtil.out("获取工作笔记主页");
		return SUCCESS;
	}

	@Override
	public String getJsonList() {
		LogUtil.out("当前操作用户："+getLoginAccount());
		List<HealthSummarize> list = healthSummarizeService.getList(getStartPage(),getPageSize(),getLoginAccount());
		LogUtil.out(getStartPage()+"*******"+getPageSize());
		int count = healthSummarizeService.getCount(getLoginAccount());
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("total", count);
		map.put("rows", list);
		setMap(map);
		LogUtil.out(getMap().toString());
		return "json";
	}

	@Override
	public String deleteById() {
		LogUtil.out("删除操作>>>>"+model.toString());
		boolean deleteById = healthSummarizeService.deleteById(model.getId(),getLoginAccount());
		return null;
	}

	@Override
	public String save() {
		LogUtil.out("新增操作>>>>"+model.toString());
		model.setCreateTime(new Date());
		LogUtil.out("现在时间"+new Date());
		model.setAccount(getLoginAccount());
		healthSummarizeService.save(model);
		return SUCCESS;
	}

	@Override
	public String find() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String update() {
		LogUtil.out("修改操作>>>"+model.toString());
		healthSummarizeService.update(model);
		return SUCCESS;
	}

	public void setHealthSummarizeService(HealthSummarizeService healthSummarizeService) {
		this.healthSummarizeService = healthSummarizeService;
	}

	
}
