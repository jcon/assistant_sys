package action.work;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import entity.work.Note;
import service.work.NoteService;
import tools.LogUtil;
import tools.StringTools;
import action.baseAction;

/**
 * 工作笔记action类
 * @author Jcon
 *
 */
public class NoteAction extends baseAction<Note>{

	private NoteService noteService;

	@Override
	public String index(){
		if (StringTools.isNullOrBlank(getLoginAccount())) {//判断用户是否登录
			return "login";
		}
		LogUtil.out("获取工作笔记主页");
		return "success";
	}
	
	@Override
	public String getJsonList() {
		LogUtil.out("当前操作用户："+getLoginAccount());
		List<Note> list = noteService.getList(getStartPage(),getPageSize(),getLoginAccount());
		LogUtil.out(getStartPage()+"*******"+getPageSize());
		int count = noteService.getCount(getLoginAccount());
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("total", count);
		map.put("rows", list);
		setMap(map);
		LogUtil.out(getMap().toString());
		return "json";
	}

	@Override
	public String deleteById() {
		LogUtil.out(model.getId());
		boolean deleteById = noteService.deleteById(model.getId(),getLoginAccount());
		return null;
	}

	@Override
	public String save() {
		model.setCreateTime(new Date());
		model.setAccount(getLoginAccount());
		noteService.save(model);
		return SUCCESS;
	}

	@Override
	public String find() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String update() {
		LogUtil.out("用户修改操作"+model.toString());
		noteService.update(model);
		return SUCCESS;
	}

	public void setNoteService(NoteService noteService) {
		this.noteService = noteService;
	}
}
