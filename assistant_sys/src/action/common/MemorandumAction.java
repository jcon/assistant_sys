package action.common;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import service.common.MemorandumService;
import tools.LogUtil;
import entity.common.Memorandum;
import entity.work.Note;
import action.baseAction;

public class MemorandumAction extends baseAction<Memorandum>{

	private MemorandumService memorandumService;
	@Override
	public String index() {
		LogUtil.out("获取备忘录主页");
		return "success";
	}

	@Override
	public String getJsonList() {
		LogUtil.out("当前操作用户："+getLoginAccount());
		List<Note> list = memorandumService.getList(getStartPage(),getPageSize(),getLoginAccount(),model.getType());
		LogUtil.out(getStartPage()+"*******"+getPageSize());
		int count = memorandumService.getCount(getLoginAccount(),model.getType());
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("total", count);
		map.put("rows", list);
		setMap(map);
		LogUtil.out(getMap().toString());
		return "json";
	}

	@Override
	public String deleteById() {
		memorandumService.deleteById(model.getId(), model.getAccount());
		return null;
	}

	@Override
	public String save() {
		model.setCreateTime(new Date());
		model.setAccount(getLoginAccount());
		memorandumService.save(model);
		return SUCCESS;
	}

	@Override
	public String find() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String update() {
		LogUtil.out("用户修改操作"+model.toString());
		memorandumService.update(model);
		return SUCCESS;
	}

	public void setMemorandumService(MemorandumService memorandumService) {
		this.memorandumService = memorandumService;
	}

	
}
