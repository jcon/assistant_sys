package service.common.imp;

import java.util.List;

import dao.common.MemorandumDao;
import entity.common.Memorandum;
import entity.work.Note;
import service.common.MemorandumService;

public class MemorandumServiceImp implements MemorandumService {

	private MemorandumDao memorandumDao;

	@Override
	public boolean save(Memorandum memorandum) {
		return memorandumDao.save(memorandum);
	}

	@Override
	public List<Note> getList(int startPage, int pageSize, String account,String type) {
		return memorandumDao.getList(startPage, pageSize, account, type);
	}

	@Override
	public int getCount(String account, String type) {
		return memorandumDao.getCount(account, type);
	}

	@Override
	public boolean deleteById(int id, String account) {
		return memorandumDao.deleteById(id, account);
	}

	@Override
	public boolean update(Memorandum memorandum) {
		return memorandumDao.update(memorandum);
	}

	public void setMemorandumDao(MemorandumDao memorandumDao) {
		this.memorandumDao = memorandumDao;
	}

}
