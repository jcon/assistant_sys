package service.family.imp;

import java.util.List;

import dao.family.FamilyActivityDao;
import entity.family.FamilyActivity;
import service.family.FamilyActivityService;

public class FamilyActivityServiceImp implements FamilyActivityService{

	private FamilyActivityDao familyActivityDao;
	
	@Override
	public boolean save(FamilyActivity familyActivity) {
		return familyActivityDao.save(familyActivity);
	}

	@Override
	public List<FamilyActivity> getList(int startPage, int pageSize,String account) {
		return familyActivityDao.getList(startPage, pageSize, account);
	}

	@Override
	public int getCount(String account) {
		return familyActivityDao.getCount(account);
	}

	@Override
	public boolean deleteById(int id, String account) {
		return familyActivityDao.deleteById(id, account);
	}

	@Override
	public boolean update(FamilyActivity familyActivity) {
		return familyActivityDao.update(familyActivity);
	}

	public void setFamilyActivityDao(FamilyActivityDao familyActivityDao) {
		this.familyActivityDao = familyActivityDao;
	}

	
}
