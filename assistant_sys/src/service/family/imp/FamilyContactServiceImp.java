package service.family.imp;

import java.util.List;

import service.family.FamilyContactService;
import dao.family.FamilyContactDao;
import entity.family.FamilyContact;

public class FamilyContactServiceImp implements FamilyContactService{

	private FamilyContactDao familyContactDao;
	
	@Override
	public boolean save(FamilyContact familyContact) {
		return familyContactDao.save(familyContact);
	}

	@Override
	public List<FamilyContact> getList(int startPage, int pageSize,String account) {
		return familyContactDao.getList(startPage, pageSize, account);
	}

	@Override
	public int getCount(String account) {
		return familyContactDao.getCount(account);
	}

	@Override
	public boolean deleteById(int id, String account) {
		return familyContactDao.deleteById(id, account);
	}

	@Override
	public boolean update(FamilyContact familyContact) {
		return familyContactDao.update(familyContact);
	}

	public void setFamilyContactDao(FamilyContactDao familyContactDao) {
		this.familyContactDao = familyContactDao;
	}

	
}
