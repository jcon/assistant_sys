package service.sys.imp;

import dao.sys.AccountDao;
import entity.sys.Account;
import service.sys.AccountService;

public class AccountServiceImp implements AccountService{

	private AccountDao accountDao;
	@Override
	public boolean save(Account account) {
		return accountDao.save(account);
	}
	
	@Override
	public Account findByAccount(String account) {
		return accountDao.findByAccount(account);
	}

	@Override
	public boolean update(Account account) {
		// TODO Auto-generated method stub
		return false;
	}

	public void setAccountDao(AccountDao accountDao) {
		this.accountDao = accountDao;
	}
}
