package service.sys;

import entity.sys.Account;

public interface AccountService {

	public boolean save(Account account);
	
	public Account findByAccount(String account);
	
	public boolean update(Account account);
	
}
