package service.life.imp;

import java.util.List;

import service.life.LifeDisportService;
import dao.life.LifeDisportDao;
import entity.life.LifeDisport;

public class LifeDisportServiceImp implements LifeDisportService {

	private LifeDisportDao lifeDisportDao;

	@Override
	public boolean save(LifeDisport lifeDisport) {
		return lifeDisportDao.save(lifeDisport);
	}

	@Override
	public List<LifeDisport> getList(int startPage, int pageSize, String account) {
		return lifeDisportDao.getList(startPage, pageSize, account);
	}

	@Override
	public int getCount(String account) {
		return lifeDisportDao.getCount(account);
	}

	@Override
	public boolean deleteById(int id, String account) {
		return lifeDisportDao.deleteById(id, account);
	}

	@Override
	public boolean update(LifeDisport lifeDisport) {
		return lifeDisportDao.update(lifeDisport);
	}

	public void setLifeDisportDao(LifeDisportDao lifeDisportDao) {
		this.lifeDisportDao = lifeDisportDao;
	}

}
