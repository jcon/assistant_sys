package service.life;

import java.util.List;

import entity.life.LifeDisport;

public interface LifeDisportService {

	public boolean save(LifeDisport lifeDisport);

	public List<LifeDisport> getList(int startPage, int pageSize, String account);

	public int getCount(String account);

	public boolean deleteById(int id, String account);

	public boolean update(LifeDisport lifeDisport);

}
