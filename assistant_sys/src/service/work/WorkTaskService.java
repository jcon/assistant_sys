package service.work;

import java.util.List;

import entity.work.WorkTask;

public interface WorkTaskService {

	public boolean save(WorkTask workTask);

	public List<WorkTask> getList(int startPage, int pageSize, String account);

	public int getCount(String account);

	public boolean deleteById(int id, String account);

	public boolean update(WorkTask workTask);

}
