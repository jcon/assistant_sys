package service.work;

import java.util.List;

import entity.work.WorkPlan;

public interface WorkPlanService {

	public boolean save(WorkPlan workPlan);

	public List<WorkPlan> getList(int startPage, int pageSize, String account);

	public int getCount(String account);

	public boolean deleteById(int id, String account);

	public boolean update(WorkPlan workPlan);

}
