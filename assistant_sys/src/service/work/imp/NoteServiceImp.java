package service.work.imp;

import java.util.List;

import dao.work.NoteDao;
import entity.work.Note;
import service.work.NoteService;

public class NoteServiceImp implements NoteService{

	private NoteDao noteDao;
	
	@Override
	public boolean save(Note note) {
		return noteDao.save(note);
	}

	@Override
	public List<Note> getList(int startPage, int pageSize,String account) {
		return noteDao.getList(startPage, pageSize,account);
	}

	@Override
	public int getCount(String account) {
		return noteDao.getCount(account);
	}

	@Override
	public boolean deleteById(int id,String account) {
		return noteDao.deleteById(id,account);
	}

	@Override
	public boolean update(Note note) {
		return noteDao.update(note);
	}
	public void setNoteDao(NoteDao noteDao) {
		this.noteDao = noteDao;
	}
}
