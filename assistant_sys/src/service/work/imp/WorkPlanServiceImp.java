package service.work.imp;

import java.util.List;

import dao.work.WorkPlanDao;
import entity.work.WorkPlan;
import service.work.WorkPlanService;

public class WorkPlanServiceImp implements WorkPlanService{

	private WorkPlanDao workPlanDao;
	
	@Override
	public boolean save(WorkPlan workPlan) {
		return workPlanDao.save(workPlan);
	}

	@Override
	public List<WorkPlan> getList(int startPage, int pageSize, String account) {
		return workPlanDao.getList(startPage, pageSize, account);
	}

	@Override
	public int getCount(String account) {
		return workPlanDao.getCount(account);
	}

	@Override
	public boolean deleteById(int id, String account) {
		return workPlanDao.deleteById(id, account);
	}

	@Override
	public boolean update(WorkPlan workPlan) {
		return workPlanDao.update(workPlan);
	}

	public void setWorkPlanDao(WorkPlanDao workPlanDao) {
		this.workPlanDao = workPlanDao;
	}

	
}
