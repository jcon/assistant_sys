package service.work.imp;

import java.util.List;

import dao.work.WorkTaskDao;
import entity.work.WorkTask;
import service.work.WorkTaskService;

public class WorkTaskServiceImp implements WorkTaskService{

	private WorkTaskDao workTaskDao;
	@Override
	public boolean save(WorkTask workTask) {
		return workTaskDao.save(workTask);
	}

	@Override
	public List<WorkTask> getList(int startPage, int pageSize, String account) {
		return workTaskDao.getList(startPage, pageSize, account);
	}

	@Override
	public int getCount(String account) {
		return workTaskDao.getCount(account);
	}

	@Override
	public boolean deleteById(int id, String account) {
		return workTaskDao.deleteById(id, account);
	}

	@Override
	public boolean update(WorkTask workTask) {
		return workTaskDao.update(workTask);
	}

	public void setWorkTaskDao(WorkTaskDao workTaskDao) {
		this.workTaskDao = workTaskDao;
	}

	
}
