package service.study;

import java.util.List;

import entity.study.StudyQuestion;

public interface StudyQuestionService {

	public boolean save(StudyQuestion studyQuestion);
	
	public List<StudyQuestion> getList(int startPage,int pageSize,String account);
	
	public int getCount(String account);
	
	public boolean deleteById(int id,String account);
	
	public boolean update(StudyQuestion studyQuestion);
}
