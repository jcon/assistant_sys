package service.study.imp;

import java.util.List;

import dao.study.StudyNoteDao;
import entity.study.StudyNote;
import service.study.StudyNoteService;

public class StudyNoteServiceImp implements StudyNoteService{

	private StudyNoteDao studyNoteDao;
	
	@Override
	public boolean save(StudyNote studyNote) {
		return studyNoteDao.save(studyNote);
	}

	@Override
	public List<StudyNote> getList(int startPage, int pageSize, String account) {
		return studyNoteDao.getList(startPage, pageSize, account);
	}

	@Override
	public int getCount(String account) {
		return studyNoteDao.getCount(account);
	}

	@Override
	public boolean deleteById(int id, String account) {
		return studyNoteDao.deleteById(id, account);
	}

	@Override
	public boolean update(StudyNote studyNote) {
		return studyNoteDao.update(studyNote);
	}

	public void setStudyNoteDao(StudyNoteDao studyNoteDao) {
		this.studyNoteDao = studyNoteDao;
	}

	
}
