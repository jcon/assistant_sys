package service.study.imp;

import java.util.List;

import dao.study.StudyTaskDao;
import entity.study.StudyTask;
import service.study.StudyTaskService;

public class StudyTaskServiceImp implements StudyTaskService{
	
	private StudyTaskDao studyTaskDao;

	@Override
	public boolean save(StudyTask studyTask) {
		return studyTaskDao.save(studyTask);
	}

	@Override
	public List<StudyTask> getList(int startPage, int pageSize, String account) {
		return studyTaskDao.getList(startPage, pageSize, account);
	}

	@Override
	public int getCount(String account) {
		return studyTaskDao.getCount(account);
	}

	@Override
	public boolean deleteById(int id, String account) {
		return studyTaskDao.deleteById(id, account);
	}

	@Override
	public boolean update(StudyTask studyTask) {
		return studyTaskDao.update(studyTask);
	}

	public void setStudyTaskDao(StudyTaskDao studyTaskDao) {
		this.studyTaskDao = studyTaskDao;
	}

	
}
