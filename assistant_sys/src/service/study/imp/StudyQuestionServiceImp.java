package service.study.imp;

import java.util.List;

import dao.study.StudyQuestionDao;
import entity.study.StudyQuestion;
import service.study.StudyQuestionService;

public class StudyQuestionServiceImp implements StudyQuestionService{

	private StudyQuestionDao studyQuestionDao;
	
	@Override
	public boolean save(StudyQuestion studyQuestion) {
		return studyQuestionDao.save(studyQuestion);
	}

	@Override
	public List<StudyQuestion> getList(int startPage, int pageSize,String account) {
		return studyQuestionDao.getList(startPage, pageSize, account);
	}

	@Override
	public int getCount(String account) {
		return studyQuestionDao.getCount(account);
	}

	@Override
	public boolean deleteById(int id, String account) {
		return studyQuestionDao.deleteById(id, account);
	}

	@Override
	public boolean update(StudyQuestion studyQuestion) {
		return studyQuestionDao.update(studyQuestion);
	}

	public void setStudyQuestionDao(StudyQuestionDao studyQuestionDao) {
		this.studyQuestionDao = studyQuestionDao;
	}

	
}
