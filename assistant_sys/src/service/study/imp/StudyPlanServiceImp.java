package service.study.imp;

import java.util.List;

import dao.study.StudyPlanDao;
import entity.study.StudyPlan;
import service.study.StudyPlanService;

public class StudyPlanServiceImp implements StudyPlanService{
	
	private StudyPlanDao studyPlanDao;

	@Override
	public boolean save(StudyPlan studyPlan) {
		return studyPlanDao.save(studyPlan);
	}

	@Override
	public List<StudyPlan> getList(int startPage, int pageSize, String account) {
		return studyPlanDao.getList(startPage, pageSize, account);
	}

	@Override
	public int getCount(String account) {
		return studyPlanDao.getCount(account);
	}

	@Override
	public boolean deleteById(int id, String account) {
		return studyPlanDao.deleteById(id, account);
	}

	@Override
	public boolean update(StudyPlan studyPlan) {
		return studyPlanDao.update(studyPlan);
	}

	public void setStudyPlanDao(StudyPlanDao studyPlanDao) {
		this.studyPlanDao = studyPlanDao;
	}

	
}
