package service.sociality.imp;

import java.util.List;

import dao.sociality.SocialityActivityDao;
import entity.sociality.SocialityActivity;
import service.sociality.SocialityActivityService;

public class SocialityActivityServiceImp implements SocialityActivityService{

	private SocialityActivityDao socialityActivityDao;
	
	@Override
	public boolean save(SocialityActivity socialityActivity) {
		return socialityActivityDao.save(socialityActivity);
	}

	@Override
	public List<SocialityActivity> getList(int startPage, int pageSize,String account) {
		return socialityActivityDao.getList(startPage, pageSize, account);
	}

	@Override
	public int getCount(String account) {
		return socialityActivityDao.getCount(account);
	}

	@Override
	public boolean deleteById(int id, String account) {
		return socialityActivityDao.deleteById(id, account);
	}

	@Override
	public boolean update(SocialityActivity socialityActivity) {
		return socialityActivityDao.update(socialityActivity);
	}

	public void setSocialityActivityDao(SocialityActivityDao socialityActivityDao) {
		this.socialityActivityDao = socialityActivityDao;
	}

	
}
