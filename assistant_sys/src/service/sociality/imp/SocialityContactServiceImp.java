package service.sociality.imp;

import java.util.List;

import dao.sociality.SocialityContactDao;
import entity.sociality.SocialityContact;
import service.sociality.SocialityContactService;

public class SocialityContactServiceImp implements SocialityContactService{

	private SocialityContactDao socialityContactDao;
	
	@Override
	public boolean save(SocialityContact socialityContact) {
		return socialityContactDao.save(socialityContact);
	}

	@Override
	public List<SocialityContact> getList(int startPage, int pageSize,String account) {
		return socialityContactDao.getList(startPage, pageSize, account);
	}

	@Override
	public int getCount(String account) {
		return socialityContactDao.getCount(account);
	}

	@Override
	public boolean deleteById(int id, String account) {
		return socialityContactDao.deleteById(id, account);
	}

	@Override
	public boolean update(SocialityContact socialityContact) {
		return socialityContactDao.update(socialityContact);
	}

	public void setSocialityContactDao(SocialityContactDao socialityContactDao) {
		this.socialityContactDao = socialityContactDao;
	}

	
}
