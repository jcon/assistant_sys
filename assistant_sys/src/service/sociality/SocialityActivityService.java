package service.sociality;

import java.util.List;

import entity.sociality.SocialityActivity;
import entity.sociality.SocialityContact;

public interface SocialityActivityService {

	public boolean save(SocialityActivity socialityActivity);
	
	public List<SocialityActivity> getList(int startPage,int pageSize,String account);
	
	public int getCount(String account);
	
	public boolean deleteById(int id,String account);
	
	public boolean update(SocialityActivity socialityActivity);
}
