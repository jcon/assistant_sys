package service.health;

import java.util.List;

import entity.health.HealthCaseHistory;

public interface HealthCaseHistoryService {

	public boolean save(HealthCaseHistory healthCaseHistory);

	public List<HealthCaseHistory> getList(int startPage, int pageSize, String account);

	public int getCount(String account);

	public boolean deleteById(int id, String account);

	public boolean update(HealthCaseHistory healthCaseHistory);



}
