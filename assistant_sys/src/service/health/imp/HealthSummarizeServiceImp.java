package service.health.imp;

import java.util.List;

import dao.health.HealthSummarizeDao;
import entity.health.HealthSummarize;
import service.health.HealthSummarizeService;

public class HealthSummarizeServiceImp implements HealthSummarizeService{

	private HealthSummarizeDao healthSummarizeDao;
	
	@Override
	public boolean save(HealthSummarize healthSummarize) {
		return healthSummarizeDao.save(healthSummarize);
	}

	@Override
	public List<HealthSummarize> getList(int startPage, int pageSize,String account) {
		return healthSummarizeDao.getList(startPage, pageSize, account);
	}

	@Override
	public int getCount(String account) {
		return healthSummarizeDao.getCount(account);
	}

	@Override
	public boolean deleteById(int id, String account) {
		return healthSummarizeDao.deleteById(id, account);
	}

	@Override
	public boolean update(HealthSummarize healthSummarize) {
		return healthSummarizeDao.update(healthSummarize);
	}

	public void setHealthSummarizeDao(HealthSummarizeDao healthSummarizeDao) {
		this.healthSummarizeDao = healthSummarizeDao;
	}

	
}
