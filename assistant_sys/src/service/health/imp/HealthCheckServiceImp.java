package service.health.imp;

import java.util.List;

import service.health.HealthCheckService;
import dao.health.HealthCheckDao;
import entity.health.HealthCheck;

public class HealthCheckServiceImp implements HealthCheckService{

	private HealthCheckDao HealthCheckDao;
	
	@Override
	public boolean save(HealthCheck healthCheck) {
		return HealthCheckDao.save(healthCheck);
	}

	@Override
	public List<HealthCheck> getList(int startPage, int pageSize, String account) {
		return HealthCheckDao.getList(startPage, pageSize, account);
	}

	@Override
	public int getCount(String account) {
		return HealthCheckDao.getCount(account);
	}

	@Override
	public boolean deleteById(int id, String account) {
		return HealthCheckDao.deleteById(id, account);
	}

	@Override
	public boolean update(HealthCheck healthCheck) {
		return HealthCheckDao.update(healthCheck);
	}

	public void setHealthCheckDao(HealthCheckDao healthCheckDao) {
		HealthCheckDao = healthCheckDao;
	}

	
	
}
