package service.health.imp;

import java.util.List;

import dao.health.HealthSportsDao;
import entity.health.HealthSports;
import service.health.HealthSportsService;

public class HealthSportsServiceImp implements HealthSportsService{

	private HealthSportsDao healthSportsDao;
	
	@Override
	public boolean save(HealthSports healthSports) {
		return healthSportsDao.save(healthSports);
	}

	@Override
	public List<HealthSports> getList(int startPage, int pageSize,String account) {
		return healthSportsDao.getList(startPage, pageSize, account);
	}

	@Override
	public int getCount(String account) {
		return healthSportsDao.getCount(account);
	}

	@Override
	public boolean deleteById(int id, String account) {
		return healthSportsDao.deleteById(id, account);
	}

	@Override
	public boolean update(HealthSports healthSports) {
		return healthSportsDao.update(healthSports);
	}

	public void setHealthSportsDao(HealthSportsDao healthSportsDao) {
		this.healthSportsDao = healthSportsDao;
	}

	
	
}
