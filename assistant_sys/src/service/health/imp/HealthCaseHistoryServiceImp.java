package service.health.imp;

import java.util.List;

import dao.health.HealthCaseHistoryDao;
import entity.health.HealthCaseHistory;
import service.health.HealthCaseHistoryService;

public class HealthCaseHistoryServiceImp implements HealthCaseHistoryService{

	private HealthCaseHistoryDao healthCaseHistoryDao;
	
	@Override
	public boolean save(HealthCaseHistory healthCaseHistory) {
		return healthCaseHistoryDao.save(healthCaseHistory);
	}

	@Override
	public List<HealthCaseHistory> getList(int startPage, int pageSize,String account) {
		return healthCaseHistoryDao.getList(startPage, pageSize, account);
	}

	@Override
	public int getCount(String account) {
		return healthCaseHistoryDao.getCount(account);
	}

	@Override
	public boolean deleteById(int id, String account) {
		return healthCaseHistoryDao.deleteById(id, account);
	}

	@Override
	public boolean update(HealthCaseHistory healthCaseHistory) {
		return healthCaseHistoryDao.update(healthCaseHistory);
	}

	public void setHealthCaseHistoryDao(HealthCaseHistoryDao healthCaseHistoryDao) {
		this.healthCaseHistoryDao = healthCaseHistoryDao;
	}

	
	
}
