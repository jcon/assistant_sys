package service.health;

import java.util.List;

import entity.health.HealthSummarize;

public interface HealthSummarizeService {

	public boolean save(HealthSummarize healthSummarize);

	public List<HealthSummarize> getList(int startPage, int pageSize, String account);

	public int getCount(String account);

	public boolean deleteById(int id, String account);

	public boolean update(HealthSummarize healthSummarize);



}
