package service.contacts;

import java.util.List;

import entity.contacts.Contacts;

public interface ContactsService {

	public boolean save(Contacts contacts);

	public List<Contacts> getList(int startPage, int pageSize, String account);

	public int getCount(String account);

	public boolean deleteById(int id, String account);

	public boolean update(Contacts contacts);



}
