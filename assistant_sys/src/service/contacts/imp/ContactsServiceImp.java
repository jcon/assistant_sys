package service.contacts.imp;

import java.util.List;

import dao.contacts.ContactsDao;
import entity.contacts.Contacts;
import service.contacts.ContactsService;

public class ContactsServiceImp implements ContactsService{

	private ContactsDao contactsDao;
	
	@Override
	public boolean save(Contacts contacts) {
		return contactsDao.save(contacts);
	}

	@Override
	public List<Contacts> getList(int startPage, int pageSize, String account) {
		return contactsDao.getList(startPage, pageSize, account);
	}

	@Override
	public int getCount(String account) {
		return contactsDao.getCount(account);
	}

	@Override
	public boolean deleteById(int id, String account) {
		return contactsDao.deleteById(id, account);
	}

	@Override
	public boolean update(Contacts contacts) {
		return contactsDao.update(contacts);
	}

	public void setContactsDao(ContactsDao contactsDao) {
		this.contactsDao = contactsDao;
	}

	
}
