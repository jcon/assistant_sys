package dao.sociality;

import java.util.List;

import entity.sociality.SocialityContact;

public interface SocialityContactDao {

	public boolean save(SocialityContact socialityContact);
	
	public List<SocialityContact> getList(int startPage,int pageSize,String account);
	
	public int getCount(String account);
	
	public boolean deleteById(int id,String account);
	
	public boolean update(SocialityContact socialityContact);
}
