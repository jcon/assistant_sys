package dao.sociality.imp;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.springframework.orm.hibernate4.HibernateCallback;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;

import dao.sociality.SocialityActivityDao;
import entity.sociality.SocialityActivity;

@SuppressWarnings("unchecked")
public class SocialityActivityDaoImp extends HibernateDaoSupport implements SocialityActivityDao{

	@Override
	public boolean save(SocialityActivity socialityActivity) {
		this.getHibernateTemplate().save(socialityActivity);
		return false;
	}

	@Override
	public List<SocialityActivity> getList(final int startPage,final int pageSize,final String account) {
		return (List<SocialityActivity>)this.getHibernateTemplate().execute(new HibernateCallback() {
			@Override
			public Object doInHibernate(Session session)throws HibernateException {
				String hql = "select * from  sociality_activity as model where model.account=:account order by model.id desc";
				SQLQuery query = session.createSQLQuery(hql);
				query.addEntity(SocialityActivity.class);
				query.setParameter("account", account);
				query.setFirstResult(startPage);
				query.setMaxResults(pageSize);
				return query.list();
			}
		});
	}

	@Override
	public int getCount(final String account) {
		return this.getHibernateTemplate().execute(new HibernateCallback() {
			@Override
			public Object doInHibernate(Session session)throws HibernateException {
				String sql = "SELECT count(*) FROM sociality_activity as model where model.account=:account";
				SQLQuery query = session.createSQLQuery(sql);
				query.setParameter("account", account);
				String count = query.uniqueResult().toString();
				return Integer.parseInt(count);
			}
		});
	}

	@Override
	public boolean deleteById(int id, String account) {
		Object object = (Object) this.getHibernateTemplate().get(SocialityActivity.class, id);
		this.getHibernateTemplate().delete(object);
		return true;
	}

	@Override
	public boolean update(SocialityActivity socialityActivity) {
		this.getHibernateTemplate().update(socialityActivity);
		return false;
	}

}
