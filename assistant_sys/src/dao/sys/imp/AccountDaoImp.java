package dao.sys.imp;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.springframework.orm.hibernate4.HibernateCallback;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;

import dao.sys.AccountDao;
import entity.sys.Account;
import entity.work.Note;

public class AccountDaoImp extends HibernateDaoSupport implements AccountDao{

	@Override
	public boolean save(Account account) {
		this.getHibernateTemplate().save(account);
		return true;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Account findByAccount(final String account) {
		return (Account)this.getHibernateTemplate().execute(new HibernateCallback() {
			@Override
			public Object doInHibernate(Session session)throws HibernateException {
				String sql = "SELECT * FROM account t where t.account='"+account+"'";
				SQLQuery query = session.createSQLQuery(sql);
				query.addEntity(Account.class);
				return query.list().get(0);
			}
		});
	}

	@Override
	public boolean update(Account account) {
		// TODO Auto-generated method stub
		return false;
	}

}
