package dao.sys;

import entity.sys.Account;

public interface AccountDao {

	public boolean save(Account account);
	
	public Account findByAccount(String account);
	
	public boolean update(Account account);
	
}
