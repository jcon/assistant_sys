package dao.family.imp;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.springframework.orm.hibernate4.HibernateCallback;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;

import dao.family.FamilyContactDao;
import entity.family.FamilyContact;

@SuppressWarnings("unchecked")
public class FamilyContactDaoImp extends HibernateDaoSupport implements FamilyContactDao{

	@Override
	public boolean save(FamilyContact familyContact) {
		this.getHibernateTemplate().save(familyContact);
		return false;
	}

	@Override
	public List<FamilyContact> getList(final int startPage,final int pageSize,final String account) {
		return (List<FamilyContact>)this.getHibernateTemplate().execute(new HibernateCallback() {
			@Override
			public Object doInHibernate(Session session)throws HibernateException {
				String hql = "select * from  family_contact as model where model.account=:account order by model.id desc";
				SQLQuery query = session.createSQLQuery(hql);
				query.addEntity(FamilyContact.class);
				query.setParameter("account", account);
				query.setFirstResult(startPage);
				query.setMaxResults(pageSize);
				return query.list();
			}
		});
	}

	@Override
	public int getCount(final String account) {
		return this.getHibernateTemplate().execute(new HibernateCallback() {
			@Override
			public Object doInHibernate(Session session)throws HibernateException {
				String sql = "SELECT count(*) FROM family_contact as model where model.account=:account";
				SQLQuery query = session.createSQLQuery(sql);
				query.setParameter("account", account);
				String count = query.uniqueResult().toString();
				return Integer.parseInt(count);
			}
		});
	}

	@Override
	public boolean deleteById(int id, String account) {
		Object object = (Object) this.getHibernateTemplate().get(FamilyContact.class, id);
		this.getHibernateTemplate().delete(object);
		return true;
	}

	@Override
	public boolean update(FamilyContact familyContact) {
		this.getHibernateTemplate().update(familyContact);
		return false;
	}

}
