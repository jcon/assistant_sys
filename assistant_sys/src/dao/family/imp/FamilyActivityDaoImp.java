package dao.family.imp;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.springframework.orm.hibernate4.HibernateCallback;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;

import dao.family.FamilyActivityDao;
import entity.family.FamilyActivity;

@SuppressWarnings("unchecked")
public class FamilyActivityDaoImp extends HibernateDaoSupport implements FamilyActivityDao{

	@Override
	public boolean save(FamilyActivity familyActivity) {
		this.getHibernateTemplate().save(familyActivity);
		return false;
	}

	@Override
	public List<FamilyActivity> getList(final int startPage,final int pageSize,final String account) {
		return (List<FamilyActivity>)this.getHibernateTemplate().execute(new HibernateCallback() {
			@Override
			public Object doInHibernate(Session session)throws HibernateException {
				String hql = "select * from  family_activity as model where model.account=:account order by model.id desc";
				SQLQuery query = session.createSQLQuery(hql);
				query.addEntity(FamilyActivity.class);
				query.setParameter("account", account);
				query.setFirstResult(startPage);
				query.setMaxResults(pageSize);
				return query.list();
			}
		});
	}

	@Override
	public int getCount(final String account) {
		return this.getHibernateTemplate().execute(new HibernateCallback() {
			@Override
			public Object doInHibernate(Session session)throws HibernateException {
				String sql = "SELECT count(*) FROM family_activity as model where model.account=:account";
				SQLQuery query = session.createSQLQuery(sql);
				query.setParameter("account", account);
				String count = query.uniqueResult().toString();
				return Integer.parseInt(count);
			}
		});
	}

	@Override
	public boolean deleteById(int id, String account) {
		Object object = (Object) this.getHibernateTemplate().get(FamilyActivity.class, id);
		this.getHibernateTemplate().delete(object);
		return true;
	}

	@Override
	public boolean update(FamilyActivity familyActivity) {
		this.getHibernateTemplate().update(familyActivity);
		return false;
	}

}
