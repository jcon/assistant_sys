package dao.family;

import java.util.List;

import entity.family.FamilyContact;

public interface FamilyContactDao {

	public boolean save(FamilyContact familyContact);

	public List<FamilyContact> getList(int startPage, int pageSize, String account);

	public int getCount(String account);

	public boolean deleteById(int id, String account);

	public boolean update(FamilyContact familyContact);



}
