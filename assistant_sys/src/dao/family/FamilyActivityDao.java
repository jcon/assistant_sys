package dao.family;

import java.util.List;

import entity.family.FamilyActivity;

public interface FamilyActivityDao {

	public boolean save(FamilyActivity familyActivity);

	public List<FamilyActivity> getList(int startPage, int pageSize, String account);

	public int getCount(String account);

	public boolean deleteById(int id, String account);

	public boolean update(FamilyActivity familyActivity);



}
