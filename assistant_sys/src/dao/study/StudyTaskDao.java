package dao.study;

import java.util.List;

import entity.study.StudyTask;

public interface StudyTaskDao {

	public boolean save(StudyTask studyTask);
	
	public List<StudyTask> getList(int startPage,int pageSize,String account);
	
	public int getCount(String account);
	
	public boolean deleteById(int id,String account);
	
	public boolean update(StudyTask studyTask);
}
