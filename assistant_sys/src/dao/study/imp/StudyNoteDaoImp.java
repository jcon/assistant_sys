package dao.study.imp;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.springframework.orm.hibernate4.HibernateCallback;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;

import dao.study.StudyNoteDao;
import entity.study.StudyNote;

@SuppressWarnings("unchecked")
public class StudyNoteDaoImp extends HibernateDaoSupport implements StudyNoteDao{

	@Override
	public boolean save(StudyNote studyNote) {
		this.getHibernateTemplate().save(studyNote);
		return false;
	}

	@Override
	public List<StudyNote> getList(final int startPage,final int pageSize,final String account) {
		return (List<StudyNote>)this.getHibernateTemplate().execute(new HibernateCallback() {
			@Override
			public Object doInHibernate(Session session)throws HibernateException {
				String hql = "select * from  study_note as model where model.account=:account order by model.id desc";
				SQLQuery query = session.createSQLQuery(hql);
				query.addEntity(StudyNote.class);
				query.setParameter("account", account);
				query.setFirstResult(startPage);
				query.setMaxResults(pageSize);
				return query.list();
			}
		});
	}

	@Override
	public int getCount(final String account) {
		return this.getHibernateTemplate().execute(new HibernateCallback() {
			@Override
			public Object doInHibernate(Session session)throws HibernateException {
				String sql = "SELECT count(*) FROM study_note as model where model.account=:account";
				SQLQuery query = session.createSQLQuery(sql);
				query.setParameter("account", account);
				String count = query.uniqueResult().toString();
				return Integer.parseInt(count);
			}
		});
	}

	@Override
	public boolean deleteById(int id, String account) {
		Object object = (Object) this.getHibernateTemplate().get(StudyNote.class, id);
		this.getHibernateTemplate().delete(object);
		return true;
	}

	@Override
	public boolean update(StudyNote studyNote) {
		this.getHibernateTemplate().update(studyNote);
		return false;
	}

}
