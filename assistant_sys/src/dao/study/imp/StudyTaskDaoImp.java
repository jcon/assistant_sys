package dao.study.imp;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.springframework.orm.hibernate4.HibernateCallback;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;

import dao.study.StudyTaskDao;
import entity.study.StudyTask;

@SuppressWarnings("unchecked")
public class StudyTaskDaoImp extends HibernateDaoSupport implements StudyTaskDao{

	@Override
	public boolean save(StudyTask studyTask) {
		this.getHibernateTemplate().save(studyTask);
		return false;
	}

	@Override
	public List<StudyTask> getList(final int startPage,final int pageSize,final String account) {
		return (List<StudyTask>)this.getHibernateTemplate().execute(new HibernateCallback() {
			@Override
			public Object doInHibernate(Session session)throws HibernateException {
				String hql = "select * from  study_task as model where model.account=:account order by model.id desc";
				SQLQuery query = session.createSQLQuery(hql);
				query.addEntity(StudyTask.class);
				query.setParameter("account", account);
				query.setFirstResult(startPage);
				query.setMaxResults(pageSize);
				return query.list();
			}
		});
	}

	@Override
	public int getCount(final String account) {
		return this.getHibernateTemplate().execute(new HibernateCallback() {
			@Override
			public Object doInHibernate(Session session)throws HibernateException {
				String sql = "SELECT count(*) FROM study_task as model where model.account=:account";
				SQLQuery query = session.createSQLQuery(sql);
				query.setParameter("account", account);
				String count = query.uniqueResult().toString();
				return Integer.parseInt(count);
			}
		});
	}

	@Override
	public boolean deleteById(int id, String account) {
		Object object = (Object) this.getHibernateTemplate().get(StudyTask.class, id);
		this.getHibernateTemplate().delete(object);
		return true;
	}

	@Override
	public boolean update(StudyTask studyTask) {
		this.getHibernateTemplate().update(studyTask);
		return false;
	}

}
