package dao.study.imp;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.springframework.orm.hibernate4.HibernateCallback;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;

import dao.study.StudyPlanDao;
import entity.study.StudyPlan;

@SuppressWarnings("unchecked")
public class StudyPlanDaoImp extends HibernateDaoSupport implements StudyPlanDao{

	@Override
	public boolean save(StudyPlan studyPlan) {
		this.getHibernateTemplate().save(studyPlan);
		return false;
	}

	@Override
	public List<StudyPlan> getList(final int startPage,final int pageSize,final String account) {
		return (List<StudyPlan>)this.getHibernateTemplate().execute(new HibernateCallback() {
			@Override
			public Object doInHibernate(Session session)throws HibernateException {
				String hql = "select * from  study_plan as model where model.account=:account order by model.id desc";
				SQLQuery query = session.createSQLQuery(hql);
				query.addEntity(StudyPlan.class);
				query.setParameter("account", account);
				query.setFirstResult(startPage);
				query.setMaxResults(pageSize);
				return query.list();
			}
		});
	}

	@Override
	public int getCount(final String account) {
		return this.getHibernateTemplate().execute(new HibernateCallback() {
			@Override
			public Object doInHibernate(Session session)throws HibernateException {
				String sql = "SELECT count(*) FROM study_plan as model where model.account=:account";
				SQLQuery query = session.createSQLQuery(sql);
				query.setParameter("account", account);
				String count = query.uniqueResult().toString();
				return Integer.parseInt(count);
			}
		});
	}

	@Override
	public boolean deleteById(int id, String account) {
		Object object = (Object) this.getHibernateTemplate().get(StudyPlan.class, id);
		this.getHibernateTemplate().delete(object);
		return true;
	}

	@Override
	public boolean update(StudyPlan studyPlan) {
		this.getHibernateTemplate().update(studyPlan);
		return false;
	}

}
