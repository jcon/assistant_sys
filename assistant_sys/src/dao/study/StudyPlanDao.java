package dao.study;

import java.util.List;

import entity.study.StudyPlan;

public interface StudyPlanDao {

	public boolean save(StudyPlan studyPlan);
	
	public List<StudyPlan> getList(int startPage,int pageSize,String account);
	
	public int getCount(String account);
	
	public boolean deleteById(int id,String account);
	
	public boolean update(StudyPlan studyPlan);
}
