package dao.study;

import java.util.List;

import entity.study.StudyNote;

public interface StudyNoteDao {

	public boolean save(StudyNote studyNote);
	
	public List<StudyNote> getList(int startPage,int pageSize,String account);
	
	public int getCount(String account);
	
	public boolean deleteById(int id,String account);
	
	public boolean update(StudyNote studyNote);
}
