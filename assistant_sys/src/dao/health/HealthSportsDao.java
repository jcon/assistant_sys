package dao.health;

import java.util.List;

import entity.health.HealthSports;

public interface HealthSportsDao {

	public boolean save(HealthSports healthSports);

	public List<HealthSports> getList(int startPage, int pageSize, String account);

	public int getCount(String account);

	public boolean deleteById(int id, String account);

	public boolean update(HealthSports healthSports);



}
