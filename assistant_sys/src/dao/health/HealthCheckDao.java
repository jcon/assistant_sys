package dao.health;

import java.util.List;

import entity.health.HealthCheck;

public interface HealthCheckDao {

	public boolean save(HealthCheck healthCheck);

	public List<HealthCheck> getList(int startPage, int pageSize, String account);

	public int getCount(String account);

	public boolean deleteById(int id, String account);

	public boolean update(HealthCheck healthCheck);



}
