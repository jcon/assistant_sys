package dao.health.imp;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.springframework.orm.hibernate4.HibernateCallback;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;

import dao.health.HealthCaseHistoryDao;
import entity.health.HealthCaseHistory;

@SuppressWarnings("unchecked")
public class HealthCaseHistoryDaoImp extends HibernateDaoSupport implements HealthCaseHistoryDao{

	@Override
	public boolean save(HealthCaseHistory healthCaseHistory) {
		this.getHibernateTemplate().save(healthCaseHistory);
		return false;
	}

	@Override
	public List<HealthCaseHistory> getList(final int startPage,final int pageSize,final String account) {
		return (List<HealthCaseHistory>)this.getHibernateTemplate().execute(new HibernateCallback() {
			@Override
			public Object doInHibernate(Session session)throws HibernateException {
				String hql = "select * from  health_case_history as model where model.account=:account order by model.id desc";
				SQLQuery query = session.createSQLQuery(hql);
				query.addEntity(HealthCaseHistory.class);
				query.setParameter("account", account);
				query.setFirstResult(startPage);
				query.setMaxResults(pageSize);
				return query.list();
			}
		});
	}

	@Override
	public int getCount(final String account) {
		return this.getHibernateTemplate().execute(new HibernateCallback() {
			@Override
			public Object doInHibernate(Session session)throws HibernateException {
				String sql = "SELECT count(*) FROM health_case_history as model where model.account=:account";
				SQLQuery query = session.createSQLQuery(sql);
				query.setParameter("account", account);
				String count = query.uniqueResult().toString();
				return Integer.parseInt(count);
			}
		});
	}

	@Override
	public boolean deleteById(int id, String account) {
		Object object = (Object) this.getHibernateTemplate().get(HealthCaseHistory.class, id);
		this.getHibernateTemplate().delete(object);
		return true;
	}

	@Override
	public boolean update(HealthCaseHistory healthCaseHistory) {
		this.getHibernateTemplate().update(healthCaseHistory);
		return false;
	}

}
