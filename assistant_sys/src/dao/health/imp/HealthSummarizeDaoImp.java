package dao.health.imp;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.springframework.orm.hibernate4.HibernateCallback;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;

import dao.health.HealthSummarizeDao;
import entity.health.HealthSummarize;

@SuppressWarnings("unchecked")
public class HealthSummarizeDaoImp extends HibernateDaoSupport implements HealthSummarizeDao{

	@Override
	public boolean save(HealthSummarize healthSummarize) {
		this.getHibernateTemplate().save(healthSummarize);
		return false;
	}

	@Override
	public List<HealthSummarize> getList(final int startPage,final int pageSize,final String account) {
		return (List<HealthSummarize>)this.getHibernateTemplate().execute(new HibernateCallback() {
			@Override
			public Object doInHibernate(Session session)throws HibernateException {
				String hql = "select * from  health_summarize as model where model.account=:account order by model.id desc";
				SQLQuery query = session.createSQLQuery(hql);
				query.addEntity(HealthSummarize.class);
				query.setParameter("account", account);
				query.setFirstResult(startPage);
				query.setMaxResults(pageSize);
				return query.list();
			}
		});
	}

	@Override
	public int getCount(final String account) {
		return this.getHibernateTemplate().execute(new HibernateCallback() {
			@Override
			public Object doInHibernate(Session session)throws HibernateException {
				String sql = "SELECT count(*) FROM health_summarize as model where model.account=:account";
				SQLQuery query = session.createSQLQuery(sql);
				query.setParameter("account", account);
				String count = query.uniqueResult().toString();
				return Integer.parseInt(count);
			}
		});
	}

	@Override
	public boolean deleteById(int id, String account) {
		Object object = (Object) this.getHibernateTemplate().get(HealthSummarize.class, id);
		this.getHibernateTemplate().delete(object);
		return true;
	}

	@Override
	public boolean update(HealthSummarize healthSummarize) {
		this.getHibernateTemplate().update(healthSummarize);
		return false;
	}

}
