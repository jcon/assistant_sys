package dao.health.imp;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.springframework.orm.hibernate4.HibernateCallback;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;

import dao.health.HealthCheckDao;
import entity.health.HealthCheck;

@SuppressWarnings("unchecked")
public class HealthCheckDaoImp extends HibernateDaoSupport implements HealthCheckDao{

	@Override
	public boolean save(HealthCheck healthCheck) {
		this.getHibernateTemplate().save(healthCheck);
		return false;
	}

	@Override
	public List<HealthCheck> getList(final int startPage,final int pageSize,final String account) {
		return (List<HealthCheck>)this.getHibernateTemplate().execute(new HibernateCallback() {
			@Override
			public Object doInHibernate(Session session)throws HibernateException {
				String hql = "select * from  health_check as model where model.account=:account order by model.id desc";
				SQLQuery query = session.createSQLQuery(hql);
				query.addEntity(HealthCheck.class);
				query.setParameter("account", account);
				query.setFirstResult(startPage);
				query.setMaxResults(pageSize);
				return query.list();
			}
		});
	}

	@Override
	public int getCount(final String account) {
		return this.getHibernateTemplate().execute(new HibernateCallback() {
			@Override
			public Object doInHibernate(Session session)throws HibernateException {
				String sql = "SELECT count(*) FROM health_check as model where model.account=:account";
				SQLQuery query = session.createSQLQuery(sql);
				query.setParameter("account", account);
				String count = query.uniqueResult().toString();
				return Integer.parseInt(count);
			}
		});
	}

	@Override
	public boolean deleteById(int id, String account) {
		Object object = (Object) this.getHibernateTemplate().get(HealthCheck.class, id);
		this.getHibernateTemplate().delete(object);
		return true;
	}

	@Override
	public boolean update(HealthCheck healthCheck) {
		this.getHibernateTemplate().update(healthCheck);
		return false;
	}

}
