package dao.contacts.imp;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.springframework.orm.hibernate4.HibernateCallback;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;

import dao.contacts.ContactsDao;
import entity.contacts.Contacts;

@SuppressWarnings("unchecked")
public class ContactsDaoImp extends HibernateDaoSupport implements ContactsDao{

	@Override
	public boolean save(Contacts contacts) {
		this.getHibernateTemplate().save(contacts);
		return false;
	}

	@Override
	public List<Contacts> getList(final int startPage,final int pageSize,final String account) {
		return (List<Contacts>)this.getHibernateTemplate().execute(new HibernateCallback() {
			@Override
			public Object doInHibernate(Session session)throws HibernateException {
				String hql = "select * from  contacts as model where model.account=:account order by model.id desc";
				SQLQuery query = session.createSQLQuery(hql);
				query.addEntity(Contacts.class);
				query.setParameter("account", account);
				query.setFirstResult(startPage);
				query.setMaxResults(pageSize);
				return query.list();
			}
		});
	}

	@Override
	public int getCount(final String account) {
		return this.getHibernateTemplate().execute(new HibernateCallback() {
			@Override
			public Object doInHibernate(Session session)throws HibernateException {
				String sql = "SELECT count(*) FROM contacts as model where model.account=:account";
				SQLQuery query = session.createSQLQuery(sql);
				query.setParameter("account", account);
				String count = query.uniqueResult().toString();
				return Integer.parseInt(count);
			}
		});
	}

	@Override
	public boolean deleteById(int id, String account) {
		Object object = (Object) this.getHibernateTemplate().get(Contacts.class, id);
		this.getHibernateTemplate().delete(object);
		return true;
	}

	@Override
	public boolean update(Contacts contacts) {
		this.getHibernateTemplate().update(contacts);
		return false;
	}

}
