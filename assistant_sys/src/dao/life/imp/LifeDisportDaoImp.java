package dao.life.imp;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.springframework.orm.hibernate4.HibernateCallback;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;

import dao.life.LifeDisportDao;
import entity.life.LifeDisport;

@SuppressWarnings("unchecked")
public class LifeDisportDaoImp extends HibernateDaoSupport implements LifeDisportDao{

	@Override
	public boolean save(LifeDisport lifeDisport) {
		this.getHibernateTemplate().save(lifeDisport);
		return false;
	}

	@Override
	public List<LifeDisport> getList(final int startPage,final int pageSize,final String account) {
		return (List<LifeDisport>)this.getHibernateTemplate().execute(new HibernateCallback() {
			@Override
			public Object doInHibernate(Session session)throws HibernateException {
				String hql = "select * from  life_disport as model where model.account=:account order by model.id desc";
				SQLQuery query = session.createSQLQuery(hql);
				query.addEntity(LifeDisport.class);
				query.setParameter("account", account);
				query.setFirstResult(startPage);
				query.setMaxResults(pageSize);
				return query.list();
			}
		});
	}

	@Override
	public int getCount(final String account) {
		return this.getHibernateTemplate().execute(new HibernateCallback() {
			@Override
			public Object doInHibernate(Session session)throws HibernateException {
				String sql = "SELECT count(*) FROM life_disport as model where model.account=:account";
				SQLQuery query = session.createSQLQuery(sql);
				query.setParameter("account", account);
				String count = query.uniqueResult().toString();
				return Integer.parseInt(count);
			}
		});
	}

	@Override
	public boolean deleteById(int id, String account) {
		Object object = (Object) this.getHibernateTemplate().get(LifeDisport.class, id);
		this.getHibernateTemplate().delete(object);
		return true;
	}

	@Override
	public boolean update(LifeDisport lifeDisport) {
		this.getHibernateTemplate().update(lifeDisport);
		return false;
	}

}
