package dao.common;

import java.util.List;

import entity.common.Memorandum;
import entity.work.Note;

public interface MemorandumDao {

	public boolean save(Memorandum memorandum);
	
	public List<Note> getList(int startPage,int pageSize,String account,String type);
	
	public int getCount(String account,String type);
	
	public boolean deleteById(int id,String account);
	
	public boolean update(Memorandum memorandum);
}
