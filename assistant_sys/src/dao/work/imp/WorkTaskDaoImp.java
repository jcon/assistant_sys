package dao.work.imp;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.springframework.orm.hibernate4.HibernateCallback;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;

import dao.work.WorkTaskDao;
import entity.work.WorkPlan;
import entity.work.WorkTask;

@SuppressWarnings("unchecked")
public class WorkTaskDaoImp extends HibernateDaoSupport implements WorkTaskDao{

	@Override
	public boolean save(WorkTask workTask) {
		this.getHibernateTemplate().save(workTask);
		return false;
	}

	@Override
	public List<WorkTask> getList(final int startPage,final int pageSize,final String account) {
		return (List<WorkTask>)this.getHibernateTemplate().execute(new HibernateCallback() {
			@Override
			public Object doInHibernate(Session session)throws HibernateException {
				String hql = "select * from  work_task as model where model.account=:account order by model.id desc";
				SQLQuery query = session.createSQLQuery(hql);
				query.addEntity(WorkTask.class);
				query.setParameter("account", account);
				query.setFirstResult(startPage);
				query.setMaxResults(pageSize);
				return query.list();
			}
		});
	}

	@Override
	public int getCount(final String account) {
		return this.getHibernateTemplate().execute(new HibernateCallback() {
			@Override
			public Object doInHibernate(Session session)throws HibernateException {
				String sql = "SELECT count(*) FROM work_task as model where model.account=:account";
				SQLQuery query = session.createSQLQuery(sql);
				query.setParameter("account", account);
				String count = query.uniqueResult().toString();
				return Integer.parseInt(count);
			}
		});
	}

	@Override
	public boolean deleteById(final int id,final String account) {
		Object object = (Object) this.getHibernateTemplate().get(WorkTask.class, id);
		this.getHibernateTemplate().delete(object);
		return true;
	}

	@Override
	public boolean update(WorkTask workTask) {
		this.getHibernateTemplate().update(workTask);
		return false;
	}

}
