package dao.work.imp;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.springframework.orm.hibernate4.HibernateCallback;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;

import tools.LogUtil;
import dao.work.NoteDao;
import entity.work.Note;
@SuppressWarnings("unchecked")
public class NoteDaoImp extends HibernateDaoSupport implements NoteDao{

	@Override
	public boolean save(Note note) {
		this.getHibernateTemplate().save(note);
		return false;
	}

	@Override
	public List<Note> getList(final int startPage,final int pageSize,final String account) {
		return (List<Note>)this.getHibernateTemplate().execute(new HibernateCallback() {
			@Override
			public Object doInHibernate(Session session)throws HibernateException {
				String hql = "select * from  work_note as model where model.account=:account order by model.id desc";
				SQLQuery query = session.createSQLQuery(hql);
				query.addEntity(Note.class);
				query.setParameter("account", account);
				query.setFirstResult(startPage);
				query.setMaxResults(pageSize);
				return query.list();
			}
		});
	}

	@Override
	public int getCount(final String account) {
		return this.getHibernateTemplate().execute(new HibernateCallback() {
			@Override
			public Object doInHibernate(Session session)throws HibernateException {
				String sql = "SELECT count(*) FROM work_note as model where model.account=:account";
				SQLQuery query = session.createSQLQuery(sql);
				query.setParameter("account", account);
				String count = query.uniqueResult().toString();
				return Integer.parseInt(count);
			}
		});
	}

	@Override
	public boolean deleteById(final int id,final String account) {
		Object object = (Object) this.getHibernateTemplate().get(Note.class, id);
		this.getHibernateTemplate().delete(object);
		return true;
	}


	@Override
	public boolean update(Note note) {
		this.getHibernateTemplate().update(note);
		return false;
	}

}
