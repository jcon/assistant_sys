package dao.work.imp;

import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.springframework.orm.hibernate4.HibernateCallback;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import dao.work.WorkPlanDao;
import entity.work.WorkPlan;

@SuppressWarnings("unchecked")
public class WorkPlanDaoImp extends HibernateDaoSupport implements WorkPlanDao{

	@Override
	public boolean save(WorkPlan workPlan) {
		this.getHibernateTemplate().save(workPlan);
		return false;
	}

	@Override
	public List<WorkPlan> getList(final int startPage, final int pageSize, final String account) {
		return (List<WorkPlan>)this.getHibernateTemplate().execute(new HibernateCallback() {
			@Override
			public Object doInHibernate(Session session)throws HibernateException {
				String hql = "select * from  work_plan as model where model.account=:account order by model.id desc";
				SQLQuery query = session.createSQLQuery(hql);
				query.addEntity(WorkPlan.class);
				query.setParameter("account", account);
				query.setFirstResult(startPage);
				query.setMaxResults(pageSize);
				return query.list();
			}
		});
	}

	@Override
	public int getCount(final String account) {
		return this.getHibernateTemplate().execute(new HibernateCallback() {
			@Override
			public Object doInHibernate(Session session)throws HibernateException {
				String sql = "SELECT count(*) FROM work_plan as model where model.account=:account";
				SQLQuery query = session.createSQLQuery(sql);
				query.setParameter("account", account);
				String count = query.uniqueResult().toString();
				return Integer.parseInt(count);
			}
		});
	}

	@Override
	public boolean deleteById(int id, String account) {
		Object object = (Object) this.getHibernateTemplate().get(WorkPlan.class, id);
		this.getHibernateTemplate().delete(object);
		return true;
	}

	@Override
	public boolean update(WorkPlan workPlan) {
		this.getHibernateTemplate().update(workPlan);
		return false;
	}

}
