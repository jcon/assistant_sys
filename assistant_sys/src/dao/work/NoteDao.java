package dao.work;

import java.util.List;

import entity.work.Note;

public interface NoteDao {

	public boolean save(Note note);
	
	public List<Note> getList(int startPage,int pageSize,String account);
	
	public int getCount(String account);
	
	public boolean deleteById(int id,String account);
	
	public boolean update(Note note);
}
