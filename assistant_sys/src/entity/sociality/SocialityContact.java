package entity.sociality;

import java.util.Date;

/**
 * 社交联系实体类
 * @author Jcon
 *
 */
public class SocialityContact {

	private Integer id;           //主键ID
	private Date createTime;      //创建时间
	private String account;       //创建用户
	private Date contactTime;     //联系时间
	private String contacts;      //联系人
	private String relation;      //关系
	private String contactWay;    //联系方式
	private String contactLength; //联系时长
	private String contactContent;//联系内容
	private String isImportant;   //是否重要
	private String remarks;       //备注
	
	public SocialityContact() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public Date getContactTime() {
		return contactTime;
	}

	public void setContactTime(Date contactTime) {
		this.contactTime = contactTime;
	}

	public String getContacts() {
		return contacts;
	}

	public void setContacts(String contacts) {
		this.contacts = contacts;
	}

	public String getRelation() {
		return relation;
	}

	public void setRelation(String relation) {
		this.relation = relation;
	}

	public String getContactWay() {
		return contactWay;
	}

	public void setContactWay(String contactWay) {
		this.contactWay = contactWay;
	}

	public String getContactLength() {
		return contactLength;
	}

	public void setContactLength(String contactLength) {
		this.contactLength = contactLength;
	}

	public String getContactContent() {
		return contactContent;
	}

	public void setContactContent(String contactContent) {
		this.contactContent = contactContent;
	}

	public String getIsImportant() {
		return isImportant;
	}

	public void setIsImportant(String isImportant) {
		this.isImportant = isImportant;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	@Override
	public String toString() {
		return "SocialityContact [id=" + id + ", createTime=" + createTime
				+ ", account=" + account + ", contactTime=" + contactTime
				+ ", contacts=" + contacts + ", relation=" + relation
				+ ", contactWay=" + contactWay + ", contactLength="
				+ contactLength + ", contactContent=" + contactContent
				+ ", isImportant=" + isImportant + ", remarks=" + remarks + "]";
	}
	
	
}
