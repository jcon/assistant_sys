package entity.sociality;

import java.util.Date;

/**
 * 社交活动实体类
 * @author Jcon
 *
 */
public class SocialityActivity {

	private Integer id;          //主键ID
	private Date createTime;     //创建时间
	private String account;      //创建用户
	private Date activityTime;   //活动时间
	private String theme;        //活动主题
	private String teammate;     //活动好友
	private String duration;     //活动时长
	private String address;      //活动地点
	private String increase;     //增长
	private String remarks;      //备注
	
	public SocialityActivity() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public Date getActivityTime() {
		return activityTime;
	}

	public void setActivityTime(Date activityTime) {
		this.activityTime = activityTime;
	}

	public String getTheme() {
		return theme;
	}

	public void setTheme(String theme) {
		this.theme = theme;
	}

	public String getTeammate() {
		return teammate;
	}

	public void setTeammate(String teammate) {
		this.teammate = teammate;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getIncrease() {
		return increase;
	}

	public void setIncrease(String increase) {
		this.increase = increase;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	@Override
	public String toString() {
		return "SocialityActivity [id=" + id + ", createTime=" + createTime
				+ ", account=" + account + ", activityTime=" + activityTime
				+ ", theme=" + theme + ", teammate=" + teammate + ", duration="
				+ duration + ", address=" + address + ", increase=" + increase
				+ ", remarks=" + remarks + "]";
	}
	
	
	
}
