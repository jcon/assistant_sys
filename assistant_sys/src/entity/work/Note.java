package entity.work;

import java.util.Date;

public class Note {

	private int id;            //主键id
	private Date createTime;   //创建时间
	private String account;    //账户
	private String content;    //内容
	private String question;   //问题
	private String notEnough;  //不足
	private String summarize;  //总结
	private String remarks;    //备注

	public Note() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getNotEnough() {
		return notEnough;
	}

	public void setNotEnough(String notEnough) {
		this.notEnough = notEnough;
	}

	public String getSummarize() {
		return summarize;
	}

	public void setSummarize(String summarize) {
		this.summarize = summarize;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	@Override
	public String toString() {
		return "Note [id=" + id + ", createTime=" + createTime + ", account="
				+ account + ", content=" + content + ", question=" + question
				+ ", notEnough=" + notEnough + ", summarize=" + summarize
				+ ", remarks=" + remarks + "]";
	}

	
}
