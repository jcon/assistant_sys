package entity.work;

import java.util.Date;

/**
 * 工作计划实体类
 * 
 * @author Jcon
 *
 */
public class WorkPlan {

	private Integer id;            // 主键id
	private Date createTime;       // 创建时间
	private String account;        // 用户
	private String planType;       // 计划类型：短、中、长期
	private String planDescribe;   // 计划描述
	private String achieveDescribe;// 达成描述(如何达成)
	private Date planTime;         // 计划时间
	private Date achieveTime;      // 实际达成时间
	private String isAchieve;      // 是否达成
	private String summary;        // 总结

	public WorkPlan() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getPlanType() {
		return planType;
	}

	public void setPlanType(String planType) {
		this.planType = planType;
	}

	public String getPlanDescribe() {
		return planDescribe;
	}

	public void setPlanDescribe(String planDescribe) {
		this.planDescribe = planDescribe;
	}

	public String getAchieveDescribe() {
		return achieveDescribe;
	}

	public void setAchieveDescribe(String achieveDescribe) {
		this.achieveDescribe = achieveDescribe;
	}

	public Date getPlanTime() {
		return planTime;
	}

	public void setPlanTime(Date planTime) {
		this.planTime = planTime;
	}

	public Date getAchieveTime() {
		return achieveTime;
	}

	public void setAchieveTime(Date achieveTime) {
		this.achieveTime = achieveTime;
	}

	public String getIsAchieve() {
		return isAchieve;
	}

	public void setIsAchieve(String isAchieve) {
		this.isAchieve = isAchieve;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	@Override
	public String toString() {
		return "WorkPlan [id=" + id + ", createTime=" + createTime
				+ ", account=" + account + ", planType=" + planType
				+ ", planDescribe=" + planDescribe + ", achieveDescribe="
				+ achieveDescribe + ", planTime=" + planTime + ", achieveTime="
				+ achieveTime + ", isAchieve=" + isAchieve + ", summary="
				+ summary + "]";
	}

	
}
