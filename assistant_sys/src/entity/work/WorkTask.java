package entity.work;

import java.util.Date;

public class WorkTask {

	private Integer id;          //主键id
	private Date createTime;     //创建时间
	private String account;      //账户
	private String taskDescribe; //任务描述
	private String taskDemand;   //任务需求
	private Date demandTime;     //要求时间
	private Date achieveTime;    //实际时间
	private String question;     //遇到问题
	private String summary;      //总结

	public WorkTask() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getTaskDescribe() {
		return taskDescribe;
	}

	public void setTaskDescribe(String taskDescribe) {
		this.taskDescribe = taskDescribe;
	}

	public String getTaskDemand() {
		return taskDemand;
	}

	public void setTaskDemand(String taskDemand) {
		this.taskDemand = taskDemand;
	}

	public Date getDemandTime() {
		return demandTime;
	}

	public void setDemandTime(Date demandTime) {
		this.demandTime = demandTime;
	}

	public Date getAchieveTime() {
		return achieveTime;
	}

	public void setAchieveTime(Date achieveTime) {
		this.achieveTime = achieveTime;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	@Override
	public String toString() {
		return "WorkTask [id=" + id + ", createTime=" + createTime
				+ ", account=" + account + ", taskDescribe=" + taskDescribe
				+ ", taskDemand=" + taskDemand + ", demandTime=" + demandTime
				+ ", achieveTime=" + achieveTime + ", question=" + question
				+ ", summary=" + summary + "]";
	}

}
