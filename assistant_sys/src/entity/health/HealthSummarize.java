package entity.health;

import java.util.Date;

/**
 * 健康概况实体类
 * 
 * @author Jcon
 *
 */
public class HealthSummarize {

	private Integer id; 		// 主键Id
	private Date createTime; 	// 创建时间
	private String account; 	// 创建用户
	private String height; 		// 身高
	private String weight; 		// 体重
	private String heartRate; 	// 心率
	private String pulmonary; 	// 肺活量
	private String bloodPressure;// 血压
	private String vision; 		// 视力
	private String bloodType;   // 血型
	private String remarks;     // 备注

	public HealthSummarize() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getHeight() {
		return height;
	}

	public void setHeight(String height) {
		this.height = height;
	}

	public String getWeight() {
		return weight;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}

	public String getHeartRate() {
		return heartRate;
	}

	public void setHeartRate(String heartRate) {
		this.heartRate = heartRate;
	}

	public String getPulmonary() {
		return pulmonary;
	}

	public void setPulmonary(String pulmonary) {
		this.pulmonary = pulmonary;
	}

	public String getBloodPressure() {
		return bloodPressure;
	}

	public void setBloodPressure(String bloodPressure) {
		this.bloodPressure = bloodPressure;
	}

	public String getVision() {
		return vision;
	}

	public void setVision(String vision) {
		this.vision = vision;
	}

	public String getBloodType() {
		return bloodType;
	}

	public void setBloodType(String bloodType) {
		this.bloodType = bloodType;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	@Override
	public String toString() {
		return "HealthSummarize [id=" + id + ", createTime=" + createTime
				+ ", account=" + account + ", height=" + height + ", weight="
				+ weight + ", heartRate=" + heartRate + ", pulmonary="
				+ pulmonary + ", bloodPressure=" + bloodPressure + ", vision="
				+ vision + ", bloodType=" + bloodType + ", remarks=" + remarks
				+ "]";
	}

}
