package entity.health;

import java.util.Date;

/**
 * 健康运动实体类
 * @author Jcon
 *
 */
public class HealthSports {

	private Integer id;          //主键id
	private Date createTime;     //创建时间
	private String account;      //创建用户
	private String sportsType;   //运动类型
	private Date sportsDate;     //运动日期
	private String sportsTimes;  //运动时长
	private String stepNumber;   //步数
	private String sportsMileage;//运动里程
	private String heartRate;    //运动后的心率
	private String remarks;      //备注
	
	public HealthSports() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getSportsType() {
		return sportsType;
	}

	public void setSportsType(String sportsType) {
		this.sportsType = sportsType;
	}

	public Date getSportsDate() {
		return sportsDate;
	}

	public void setSportsDate(Date sportsDate) {
		this.sportsDate = sportsDate;
	}

	public String getSportsTimes() {
		return sportsTimes;
	}

	public void setSportsTimes(String sportsTimes) {
		this.sportsTimes = sportsTimes;
	}

	public String getStepNumber() {
		return stepNumber;
	}

	public void setStepNumber(String stepNumber) {
		this.stepNumber = stepNumber;
	}

	public String getSportsMileage() {
		return sportsMileage;
	}

	public void setSportsMileage(String sportsMileage) {
		this.sportsMileage = sportsMileage;
	}

	public String getHeartRate() {
		return heartRate;
	}

	public void setHeartRate(String heartRate) {
		this.heartRate = heartRate;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	@Override
	public String toString() {
		return "HealthSports [id=" + id + ", createTime=" + createTime
				+ ", account=" + account + ", sportsType=" + sportsType
				+ ", sportsDate=" + sportsDate + ", sportsTimes=" + sportsTimes
				+ ", stepNumber=" + stepNumber + ", sportsMileage="
				+ sportsMileage + ", heartRate=" + heartRate + ", remarks="
				+ remarks + "]";
	}
	
	
	
}
