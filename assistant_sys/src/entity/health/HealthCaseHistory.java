package entity.health;

import java.util.Date;

/**
 * 健康病历实体类
 * 
 * @author Jcon
 *
 */
public class HealthCaseHistory {

	private Integer id;                //主键ID
	private Date createTime;           //创建时间
	private String account;            //创建用户
	private String symptom;            //症状
	private String isTakePills;        //是否吃药
	private String momenProprium;      //药名
	private String dosage;             //用量
	private String medicationDuration; //用药时长
	private String symptomaticApproach;//对症办法
	private String recoveryTime;       //恢复时间
	private String recoveryMode;       //恢复状态
	private String dietaryAttention;   //饮食注意点
	private String otherAttention;     //其他注意点
	private String remarks;            //备注
	
	public HealthCaseHistory() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getSymptom() {
		return symptom;
	}

	public void setSymptom(String symptom) {
		this.symptom = symptom;
	}

	public String getIsTakePills() {
		return isTakePills;
	}

	public void setIsTakePills(String isTakePills) {
		this.isTakePills = isTakePills;
	}

	public String getMomenProprium() {
		return momenProprium;
	}

	public void setMomenProprium(String momenProprium) {
		this.momenProprium = momenProprium;
	}

	public String getDosage() {
		return dosage;
	}

	public void setDosage(String dosage) {
		this.dosage = dosage;
	}

	public String getMedicationDuration() {
		return medicationDuration;
	}

	public void setMedicationDuration(String medicationDuration) {
		this.medicationDuration = medicationDuration;
	}

	public String getSymptomaticApproach() {
		return symptomaticApproach;
	}

	public void setSymptomaticApproach(String symptomaticApproach) {
		this.symptomaticApproach = symptomaticApproach;
	}

	public String getRecoveryTime() {
		return recoveryTime;
	}

	public void setRecoveryTime(String recoveryTime) {
		this.recoveryTime = recoveryTime;
	}

	public String getRecoveryMode() {
		return recoveryMode;
	}

	public void setRecoveryMode(String recoveryMode) {
		this.recoveryMode = recoveryMode;
	}

	public String getDietaryAttention() {
		return dietaryAttention;
	}

	public void setDietaryAttention(String dietaryAttention) {
		this.dietaryAttention = dietaryAttention;
	}

	public String getOtherAttention() {
		return otherAttention;
	}

	public void setOtherAttention(String otherAttention) {
		this.otherAttention = otherAttention;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	@Override
	public String toString() {
		return "HealthCaseHistory [id=" + id + ", createTime=" + createTime
				+ ", account=" + account + ", symptom=" + symptom
				+ ", isTakePills=" + isTakePills + ", momenProprium="
				+ momenProprium + ", dosage=" + dosage
				+ ", medicationDuration=" + medicationDuration
				+ ", symptomaticApproach=" + symptomaticApproach
				+ ", recoveryTime=" + recoveryTime + ", recoveryMode="
				+ recoveryMode + ", dietaryAttention=" + dietaryAttention
				+ ", otherAttention=" + otherAttention + ", remarks=" + remarks
				+ "]";
	}

	
}
