package entity.health;

import java.util.Date;

/**
 * 健康体检实体类
 * 
 * @author Jcon
 *
 */
public class HealthCheck {

	private Integer id;            //主键ID
	private Date createTime;       //创建时间
	private String account;        //创建用户
	private Date checkTime;        //体检时间
	private String checkProject;   //体检项目
	private String checkInfirmary; //体检医院
	private String address;        //地点
	private String checkFruit;     //体检结果
	private String standard;       //参考参数
	private String question;       //问题点
	private String countermove;    //对策调整
	private String takeCare;       //饮食注意点
	private String remarks;        //备注

	public HealthCheck() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public Date getCheckTime() {
		return checkTime;
	}

	public void setCheckTime(Date checkTime) {
		this.checkTime = checkTime;
	}

	public String getCheckProject() {
		return checkProject;
	}

	public void setCheckProject(String checkProject) {
		this.checkProject = checkProject;
	}

	public String getCheckInfirmary() {
		return checkInfirmary;
	}

	public void setCheckInfirmary(String checkInfirmary) {
		this.checkInfirmary = checkInfirmary;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCheckFruit() {
		return checkFruit;
	}

	public void setCheckFruit(String checkFruit) {
		this.checkFruit = checkFruit;
	}

	public String getStandard() {
		return standard;
	}

	public void setStandard(String standard) {
		this.standard = standard;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getCountermove() {
		return countermove;
	}

	public void setCountermove(String countermove) {
		this.countermove = countermove;
	}

	public String getTakeCare() {
		return takeCare;
	}

	public void setTakeCare(String takeCare) {
		this.takeCare = takeCare;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	@Override
	public String toString() {
		return "HealthCheck [id=" + id + ", createTime=" + createTime
				+ ", account=" + account + ", checkTime=" + checkTime
				+ ", checkProject=" + checkProject + ", checkInfirmary="
				+ checkInfirmary + ", address=" + address + ", checkFruit="
				+ checkFruit + ", standard=" + standard + ", question="
				+ question + ", countermove=" + countermove + ", takeCare="
				+ takeCare + ", remarks=" + remarks + "]";
	}

}
