package entity.life;

import java.util.Date;
/**
 * 生活娱乐实体类
 * @author Jcon
 *
 */
public class LifeDisport {

	private Integer id;           //主键id
	private Date createTime;      //创建时间
	private String account;       //用户
	private String disportType;   //娱乐类型
	private String content;       //内容
	private String consumingTime; //用时
	private String disportAddress;//娱乐地址
	private String teammate;      //队友
	private String remarks;       //备注

	public LifeDisport() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getDisportType() {
		return disportType;
	}

	public void setDisportType(String disportType) {
		this.disportType = disportType;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getConsumingTime() {
		return consumingTime;
	}

	public void setConsumingTime(String consumingTime) {
		this.consumingTime = consumingTime;
	}

	public String getDisportAddress() {
		return disportAddress;
	}

	public void setDisportAddress(String disportAddress) {
		this.disportAddress = disportAddress;
	}

	public String getTeammate() {
		return teammate;
	}

	public void setTeammate(String teammate) {
		this.teammate = teammate;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	@Override
	public String toString() {
		return "LifeDisport [id=" + id + ", createTime=" + createTime
				+ ", account=" + account + ", disportType=" + disportType
				+ ", content=" + content + ", consumingTime=" + consumingTime
				+ ", disportAddress=" + disportAddress + ", teammate="
				+ teammate + ", remarks=" + remarks + "]";
	}

}
