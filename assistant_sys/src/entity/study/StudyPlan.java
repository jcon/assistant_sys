package entity.study;

import java.util.Date;

/**
 * 学习计划实体类
 * 
 * @author Jcon
 *
 */
public class StudyPlan {

	private Integer id;        //主键ID
	private Date createTime;   //创建时间
	private String account;    //创建用户
	private String planType;   //计划类型
	private String content;    //内容
	private String achieveDescribe; //实现详情
	private Date planTime;     //计划时间
	private Date achieveTime;  //完成时间
	private String isAchieve;  //是否达成
	private String cause;      //原因
	private String remarks;    //备注

	public StudyPlan() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getPlanType() {
		return planType;
	}

	public void setPlanType(String planType) {
		this.planType = planType;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getPlanTime() {
		return planTime;
	}

	public void setPlanTime(Date planTime) {
		this.planTime = planTime;
	}

	public Date getAchieveTime() {
		return achieveTime;
	}

	public void setAchieveTime(Date achieveTime) {
		this.achieveTime = achieveTime;
	}

	public String getIsAchieve() {
		return isAchieve;
	}

	public void setIsAchieve(String isAchieve) {
		this.isAchieve = isAchieve;
	}

	public String getCause() {
		return cause;
	}

	public void setCause(String cause) {
		this.cause = cause;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getAchieveDescribe() {
		return achieveDescribe;
	}

	public void setAchieveDescribe(String achieveDescribe) {
		this.achieveDescribe = achieveDescribe;
	}

	@Override
	public String toString() {
		return "StudyPlan [id=" + id + ", createTime=" + createTime
				+ ", account=" + account + ", planType=" + planType
				+ ", content=" + content + ", achieveDescribe="
				+ achieveDescribe + ", planTime=" + planTime + ", achieveTime="
				+ achieveTime + ", isAchieve=" + isAchieve + ", cause=" + cause
				+ ", remarks=" + remarks + "]";
	}



}
