package entity.study;

import java.util.Date;

/**
 * 学习笔记实体类
 * 
 * @author Jcon
 *
 */
public class StudyNote {

	private Integer id;
	private Date createTime;
	private String account;
	private String content;
	private String summary;
	private String remarks;

	public StudyNote() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	@Override
	public String toString() {
		return "StudyNote [id=" + id + ", createTime=" + createTime
				+ ", account=" + account + ", content=" + content
				+ ", summary=" + summary + ", remarks=" + remarks + "]";
	}

}
