package entity.study;

import java.util.Date;

/**
 * 学习问题实体类
 * 
 * @author Jcon
 *
 */
public class StudyQuestion {

	private Integer id;
	private Date createTime;
	private String account;
	private String question;
	private String solveMethod;
	private String summary;
	private String remarks;

	public StudyQuestion() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getSolveMethod() {
		return solveMethod;
	}

	public void setSolveMethod(String solveMethod) {
		this.solveMethod = solveMethod;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	@Override
	public String toString() {
		return "StudyQuestion [id=" + id + ", createTime=" + createTime
				+ ", account=" + account + ", question=" + question
				+ ", solveMethod=" + solveMethod + ", summary=" + summary
				+ ", remarks=" + remarks + "]";
	}

}
