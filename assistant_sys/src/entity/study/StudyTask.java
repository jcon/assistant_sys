package entity.study;

import java.util.Date;

/**
 * 学习任务实体类
 * 
 * @author Jcon
 *
 */
public class StudyTask {

	private Integer id;           //主键id
	private Date createTime;      //创建时间
	private String account;       //创建用户
	private String taskDescribe;  //任务描述
	private String taskQuantity;  //任务量
	private Date planTime;        //计划时间
	private Date achieveTime;     //实际时间
	private String isAchieve;     //是否完成
	private String cause;         //原因
	private String remarks;       //备注

	public StudyTask() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getTaskDescribe() {
		return taskDescribe;
	}

	public void setTaskDescribe(String taskDescribe) {
		this.taskDescribe = taskDescribe;
	}

	public String getTaskQuantity() {
		return taskQuantity;
	}

	public void setTaskQuantity(String taskQuantity) {
		this.taskQuantity = taskQuantity;
	}

	public Date getPlanTime() {
		return planTime;
	}

	public void setPlanTime(Date planTime) {
		this.planTime = planTime;
	}

	public Date getAchieveTime() {
		return achieveTime;
	}

	public void setAchieveTime(Date achieveTime) {
		this.achieveTime = achieveTime;
	}

	public String getIsAchieve() {
		return isAchieve;
	}

	public void setIsAchieve(String isAchieve) {
		this.isAchieve = isAchieve;
	}

	public String getCause() {
		return cause;
	}

	public void setCause(String cause) {
		this.cause = cause;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	@Override
	public String toString() {
		return "StudyTask [id=" + id + ", createTime=" + createTime
				+ ", account=" + account + ", taskDescribe=" + taskDescribe
				+ ", taskQuantity=" + taskQuantity + ", planTime=" + planTime
				+ ", achieveTime=" + achieveTime + ", isAchieve=" + isAchieve
				+ ", cause=" + cause + ", remarks=" + remarks + "]";
	}

}
