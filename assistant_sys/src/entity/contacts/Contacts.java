package entity.contacts;

import java.util.Date;

/**
 * 联系人实体类
 * @author Jcon
 *
 */
public class Contacts {

	private Integer id;            //主键id
	private Date createTime;       //创建时间
	private String account;        //创建用户
	private String contactsName;   //联系人名字
	private String sex;            //性别
	private String relation;       //关系
	private String mobile;         //手机号
	private String qq;             //qq
	private String email;          //邮箱
	private String workAddress;    //工作地址
	private String familyAddress;  //家庭地址
	private String position;       //职位
	private String birthday;       //生日
	private String importantDegree;//重要程度
	private String remarks;        //备注
	
	public Contacts() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getContactsName() {
		return contactsName;
	}

	public void setContactsName(String contactsName) {
		this.contactsName = contactsName;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getRelation() {
		return relation;
	}

	public void setRelation(String relation) {
		this.relation = relation;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getQq() {
		return qq;
	}

	public void setQq(String qq) {
		this.qq = qq;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getWorkAddress() {
		return workAddress;
	}

	public void setWorkAddress(String workAddress) {
		this.workAddress = workAddress;
	}

	public String getFamilyAddress() {
		return familyAddress;
	}

	public void setFamilyAddress(String familyAddress) {
		this.familyAddress = familyAddress;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getImportantDegree() {
		return importantDegree;
	}

	public void setImportantDegree(String importantDegree) {
		this.importantDegree = importantDegree;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	@Override
	public String toString() {
		return "Contacts [id=" + id + ", createTime=" + createTime
				+ ", account=" + account + ", contactsName=" + contactsName
				+ ", sex=" + sex + ", relation=" + relation + ", mobile="
				+ mobile + ", qq=" + qq + ", email=" + email + ", workAddress="
				+ workAddress + ", familyAddress=" + familyAddress
				+ ", position=" + position + ", birthday=" + birthday
				+ ", importantDegree=" + importantDegree + ", remarks="
				+ remarks + "]";
	}
	
	
}
