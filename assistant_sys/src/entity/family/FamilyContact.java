package entity.family;

import java.util.Date;

/**
 * 家庭联系实体类
 * @author Jcon
 *
 */
public class FamilyContact {

	private Integer id;        //主键id
	private Date createTime;   //创建时间
	private String account;    //创建用户
	private Date contactTime;  //联系时间
	private String contacts;   //联系人
	private String contactWay; //联系方式
	private String content;    //内容
	private String contactLength;//时长 
	private String remarks;    //备注
	
	public FamilyContact() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public Date getContactTime() {
		return contactTime;
	}

	public void setContactTime(Date contactTime) {
		this.contactTime = contactTime;
	}

	public String getContacts() {
		return contacts;
	}

	public void setContacts(String contacts) {
		this.contacts = contacts;
	}

	public String getContactWay() {
		return contactWay;
	}

	public void setContactWay(String contactWay) {
		this.contactWay = contactWay;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getContactLength() {
		return contactLength;
	}

	public void setContactLength(String contactLength) {
		this.contactLength = contactLength;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	@Override
	public String toString() {
		return "FamilyContact [id=" + id + ", createTime=" + createTime
				+ ", account=" + account + ", contactTime=" + contactTime
				+ ", contacts=" + contacts + ", contactWay=" + contactWay
				+ ", content=" + content + ", contactLength=" + contactLength
				+ ", remarks=" + remarks + "]";
	}
	
	
	
}
