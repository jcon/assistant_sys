package entity.family;

import java.util.Date;

/**
 * 家庭活动实体类
 * @author Jcon
 *
 */
public class FamilyActivity {

	private Integer id;            //主键ID
	private Date createTime;       //创建时间
	private String account;        //创建用户
	private Date activityTime;     //活动时间
	private String theme;          //主题
	private String activityLength; //活动时长
	private String content;        //内容
	private String teammate;       //队友
	private String remarks;        //备注
	
	public FamilyActivity() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public Date getActivityTime() {
		return activityTime;
	}

	public void setActivityTime(Date activityTime) {
		this.activityTime = activityTime;
	}

	public String getTheme() {
		return theme;
	}

	public void setTheme(String theme) {
		this.theme = theme;
	}

	public String getActivityLength() {
		return activityLength;
	}

	public void setActivityLength(String activityLength) {
		this.activityLength = activityLength;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getTeammate() {
		return teammate;
	}

	public void setTeammate(String teammate) {
		this.teammate = teammate;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	@Override
	public String toString() {
		return "FamilyActivity [id=" + id + ", createTime=" + createTime
				+ ", account=" + account + ", activityTime=" + activityTime
				+ ", theme=" + theme + ", activityLength=" + activityLength
				+ ", content=" + content + ", teammate=" + teammate
				+ ", remarks=" + remarks + "]";
	}

	
}
