package entity.common;

import java.util.Date;

public class Memorandum {

	private int id;
	private String account;
	private Date createTime;
	private String type;
	private String content;
	private String remarks;

	public Memorandum() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	@Override
	public String toString() {
		return "Memorandum [id=" + id + ", account=" + account
				+ ", createTime=" + createTime + ", type=" + type
				+ ", content=" + content + ", remarks=" + remarks + "]";
	}

}
