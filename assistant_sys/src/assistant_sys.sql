/*
SQLyog 企业版 - MySQL GUI v8.14 
MySQL - 5.5.40 : Database - assistant_sys
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`assistant_sys` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `assistant_sys`;

/*Table structure for table `account` */

DROP TABLE IF EXISTS `account`;

CREATE TABLE `account` (
  `ID` int(11) NOT NULL COMMENT '主键',
  `CREATE_TIME` datetime DEFAULT NULL COMMENT '创建时间',
  `ACCOUNT` varchar(20) NOT NULL COMMENT '账户',
  `SEX` varchar(1) NOT NULL COMMENT '性别',
  `PASSWORD` varchar(20) NOT NULL COMMENT '密码',
  `PHONE_NUMBER` varchar(20) DEFAULT NULL COMMENT '手机号',
  `EMAIL` varchar(20) DEFAULT NULL COMMENT '邮箱',
  `STATUS` int(1) DEFAULT NULL COMMENT '状态：1可用，2不可用',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `account` */

insert  into `account`(`ID`,`CREATE_TIME`,`ACCOUNT`,`SEX`,`PASSWORD`,`PHONE_NUMBER`,`EMAIL`,`STATUS`) values (2,'2017-01-08 19:45:51','123','男','123',NULL,NULL,1),(3,'2017-01-08 20:03:52','jcon','男','admin','18899998888','525176261@qq.com',1),(7,'2017-01-11 22:00:25','123','女','123','123','123',0),(8,'2017-01-11 22:05:58','123','女','123','123','123',0),(9,'2017-01-11 22:06:54','123','女','123','123','123',0),(10,'2017-01-11 22:06:58','123','女','123','123','123',0);

/*Table structure for table `contacts` */

DROP TABLE IF EXISTS `contacts`;

CREATE TABLE `contacts` (
  `ID` int(11) NOT NULL COMMENT '主键ID',
  `CREATE_TIME` datetime DEFAULT NULL COMMENT '创建时间',
  `ACCOUNT` varchar(20) NOT NULL COMMENT '创建用户',
  `CONTACTS_NAME` varchar(20) DEFAULT NULL COMMENT '名字',
  `SEX` varchar(5) DEFAULT NULL COMMENT '性别',
  `RELATION` varchar(20) DEFAULT NULL COMMENT '关系',
  `MOBILE` varchar(11) DEFAULT NULL COMMENT '手机号',
  `QQ` varchar(20) DEFAULT NULL COMMENT 'qq',
  `EMAIL` varchar(30) DEFAULT NULL COMMENT '邮箱',
  `WORK_ADDRESS` varchar(200) DEFAULT NULL COMMENT '工作地址',
  `FAMILY_ADDRESS` varchar(200) DEFAULT NULL COMMENT '家庭地址',
  `POSITION` varchar(20) DEFAULT NULL COMMENT '职位',
  `BIRTHDAY` varchar(30) DEFAULT NULL COMMENT '生日',
  `IMPORTANT_DEGREE` varchar(20) DEFAULT NULL COMMENT '重要程度',
  `REMARKS` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `contacts` */

insert  into `contacts`(`ID`,`CREATE_TIME`,`ACCOUNT`,`CONTACTS_NAME`,`SEX`,`RELATION`,`MOBILE`,`QQ`,`EMAIL`,`WORK_ADDRESS`,`FAMILY_ADDRESS`,`POSITION`,`BIRTHDAY`,`IMPORTANT_DEGREE`,`REMARKS`) values (2,'2017-02-04 09:59:13','jcon','cc','c','c','c','c','c','c555555555','c','c','c','c','cc');

/*Table structure for table `family_activity` */

DROP TABLE IF EXISTS `family_activity`;

CREATE TABLE `family_activity` (
  `ID` int(11) NOT NULL COMMENT '主键ID',
  `CREATE_TIME` datetime DEFAULT NULL COMMENT '创建时间',
  `ACCOUNT` varchar(20) NOT NULL COMMENT '创建用户',
  `ACTIVITY_TIME` datetime DEFAULT NULL COMMENT '活动时间',
  `THEME` varchar(200) DEFAULT NULL COMMENT '主题',
  `ACTIVITY_LENGTH` varchar(20) DEFAULT NULL COMMENT '时长',
  `CONTENT` varchar(800) DEFAULT NULL COMMENT '内容',
  `TEAMMATE` varchar(200) DEFAULT NULL COMMENT '队友',
  `REMARKS` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `family_activity` */

insert  into `family_activity`(`ID`,`CREATE_TIME`,`ACCOUNT`,`ACTIVITY_TIME`,`THEME`,`ACTIVITY_LENGTH`,`CONTENT`,`TEAMMATE`,`REMARKS`) values (1,'2017-02-04 09:55:37','jcon',NULL,NULL,NULL,NULL,NULL,'d');

/*Table structure for table `family_contact` */

DROP TABLE IF EXISTS `family_contact`;

CREATE TABLE `family_contact` (
  `ID` int(11) NOT NULL COMMENT '主键ID',
  `CREATE_TIME` datetime DEFAULT NULL COMMENT '创建时间',
  `ACCOUNT` varchar(20) NOT NULL COMMENT '创建用户',
  `CONTACT_TIME` datetime DEFAULT NULL COMMENT '联系时间',
  `CONTACTS` varchar(20) DEFAULT NULL COMMENT '联系人',
  `CONTACT_WAY` varchar(20) DEFAULT NULL COMMENT '联系方式',
  `CONTENT` varchar(800) DEFAULT NULL COMMENT '内容',
  `CONTACT_LENGTH` varchar(20) DEFAULT NULL COMMENT '时长',
  `REMARKS` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `family_contact` */

insert  into `family_contact`(`ID`,`CREATE_TIME`,`ACCOUNT`,`CONTACT_TIME`,`CONTACTS`,`CONTACT_WAY`,`CONTENT`,`CONTACT_LENGTH`,`REMARKS`) values (1,'2017-02-08 20:37:47','jcon','2017-02-08 20:37:41','a','a','a','a','a');

/*Table structure for table `health_case_history` */

DROP TABLE IF EXISTS `health_case_history`;

CREATE TABLE `health_case_history` (
  `ID` int(11) NOT NULL COMMENT '主键ID',
  `CREATE_TIME` datetime DEFAULT NULL COMMENT '创建时间',
  `ACCOUNT` varchar(20) DEFAULT NULL COMMENT '创建用户',
  `SYMPTOM` varchar(800) DEFAULT NULL COMMENT '症状',
  `IS_TAKE_PILLS` varchar(20) DEFAULT NULL COMMENT '是否吃药',
  `MOMEN_PROPRIUM` varchar(200) DEFAULT NULL COMMENT '药品名称',
  `DOSAGE` varchar(200) DEFAULT NULL COMMENT '用量',
  `MEDICATION_DURATION` varchar(20) DEFAULT NULL COMMENT '用药时长',
  `SYMPTOMATIC_APPROACH` varchar(800) DEFAULT NULL COMMENT '对症办法',
  `RECOVERY_TIME` varchar(20) DEFAULT NULL COMMENT '恢复时间',
  `RECOVERY_MODE` varchar(800) DEFAULT NULL COMMENT '恢复状态',
  `DIETARY_ATTENTION` varchar(800) DEFAULT NULL COMMENT '饮食注意点',
  `OTHER_ATTENTION` varchar(800) DEFAULT NULL COMMENT '其他注意点',
  `REMARKS` varchar(500) DEFAULT NULL COMMENT '备注',
  `MODICATION_DURATION` longtext,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `health_case_history` */

/*Table structure for table `health_check` */

DROP TABLE IF EXISTS `health_check`;

CREATE TABLE `health_check` (
  `ID` int(11) NOT NULL COMMENT '主键ID',
  `CREATE_TIME` datetime DEFAULT NULL COMMENT '创建时间',
  `ACCOUNT` varchar(20) NOT NULL COMMENT '创建用户',
  `CHECK_TIME` datetime DEFAULT NULL COMMENT '体检时间',
  `CHECK_PROJECT` varchar(200) DEFAULT NULL COMMENT '体检项目',
  `CHECK_INFIRMARY` varchar(200) DEFAULT NULL COMMENT '体检医院',
  `ADDRESS` varchar(200) DEFAULT NULL COMMENT '地点',
  `CHECK_FRUIT` varchar(800) DEFAULT NULL COMMENT '体检结果',
  `STANDARD` varchar(800) DEFAULT NULL COMMENT '参考参数',
  `QUESTION` varchar(800) DEFAULT NULL COMMENT '问题点',
  `COUNTERMOVE` varchar(800) DEFAULT NULL COMMENT '对策调整',
  `TAKE_CARE` varchar(800) DEFAULT NULL COMMENT '饮食注意点',
  `REMARKS` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `health_check` */

/*Table structure for table `health_sports` */

DROP TABLE IF EXISTS `health_sports`;

CREATE TABLE `health_sports` (
  `ID` int(11) NOT NULL COMMENT '主键ID',
  `CREATE_TIME` datetime DEFAULT NULL COMMENT '创建时间',
  `ACCOUNT` varchar(20) NOT NULL COMMENT '创建用户',
  `SPORTS_TYPE` varchar(20) DEFAULT NULL COMMENT '运动类型',
  `SPORTS_DATE` datetime DEFAULT NULL COMMENT '运动时间',
  `SPORTS_TIMES` varchar(20) DEFAULT NULL COMMENT '时长',
  `STEP_NUMBER` varchar(20) DEFAULT NULL COMMENT '步数',
  `SPORTS_MILEAGE` varchar(20) DEFAULT NULL COMMENT '运动里程',
  `HEART_RATE` varchar(20) DEFAULT NULL COMMENT '运动后的心率',
  `REMARKS` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `health_sports` */

/*Table structure for table `health_summarize` */

DROP TABLE IF EXISTS `health_summarize`;

CREATE TABLE `health_summarize` (
  `ID` int(11) NOT NULL COMMENT '主键ID',
  `CREATE_TIME` datetime DEFAULT NULL COMMENT '创建时间',
  `ACCOUNT` varchar(20) NOT NULL COMMENT '创建用户',
  `HEIGHT` varchar(20) DEFAULT NULL COMMENT '身高',
  `WERGHT` varchar(20) DEFAULT NULL COMMENT '体重',
  `HEART_RATE` varchar(20) DEFAULT NULL COMMENT '心率',
  `PULMONARY` varchar(20) DEFAULT NULL COMMENT '肺活量',
  `BLOOD_PRESSURE` varchar(20) DEFAULT NULL COMMENT '血压',
  `VISION` varchar(20) DEFAULT NULL COMMENT '视力',
  `BLOOD_TYPE` varchar(20) DEFAULT NULL COMMENT '血型',
  `REMARKS` varchar(500) DEFAULT NULL COMMENT '备注',
  `WEIGHT` varchar(20) DEFAULT NULL,
  `HEART_TATE` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `health_summarize` */

insert  into `health_summarize`(`ID`,`CREATE_TIME`,`ACCOUNT`,`HEIGHT`,`WERGHT`,`HEART_RATE`,`PULMONARY`,`BLOOD_PRESSURE`,`VISION`,`BLOOD_TYPE`,`REMARKS`,`WEIGHT`,`HEART_TATE`) values (2,'2017-02-01 14:37:50','jcon','test',NULL,NULL,'test','test','test','test','sdf','tesy','test');

/*Table structure for table `life_disport` */

DROP TABLE IF EXISTS `life_disport`;

CREATE TABLE `life_disport` (
  `ID` int(11) NOT NULL COMMENT '主键ID',
  `CREATE_TIME` datetime DEFAULT NULL COMMENT '创建时间',
  `ACCOUNT` varchar(20) NOT NULL COMMENT '用户',
  `DISPORT_TYPE` varchar(20) DEFAULT NULL COMMENT '娱乐类型',
  `CONTENT` varchar(800) DEFAULT NULL COMMENT '内容',
  `CONSUMING_TIME` varchar(20) DEFAULT NULL COMMENT '娱乐用时',
  `DISPORT_ADDRESS` varchar(100) DEFAULT NULL COMMENT '娱乐地点',
  `TEAMMATE` varchar(100) DEFAULT NULL COMMENT '队友',
  `REMARKS` varchar(200) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `life_disport` */

insert  into `life_disport`(`ID`,`CREATE_TIME`,`ACCOUNT`,`DISPORT_TYPE`,`CONTENT`,`CONSUMING_TIME`,`DISPORT_ADDRESS`,`TEAMMATE`,`REMARKS`) values (2,'2017-01-25 21:27:18','jcon','游戏','去跟朋友打了一场游戏，嗨死\r\n','2H','北流、玉林、北京','me','不错'),(3,'2017-01-26 09:59:14','jcon','见朋友','跟刀郎大哥吹牛逼','1D','石窝','朱喜才','');

/*Table structure for table `memorandum` */

DROP TABLE IF EXISTS `memorandum`;

CREATE TABLE `memorandum` (
  `ID` int(11) NOT NULL COMMENT '主键ID',
  `ACCOUNT` varchar(20) NOT NULL COMMENT '创建用户',
  `CREATE_TIME` datetime DEFAULT NULL COMMENT '创建时间',
  `TYPE` varchar(10) NOT NULL COMMENT '类型：学习、工作...',
  `CONTENT` varchar(800) DEFAULT NULL COMMENT '内容',
  `REMARKS` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `memorandum` */

insert  into `memorandum`(`ID`,`ACCOUNT`,`CREATE_TIME`,`TYPE`,`CONTENT`,`REMARKS`) values (6,'jcon','2017-01-11 13:33:59','工作','d','d'),(7,'jcon','2017-01-11 13:33:59','工作','d','d'),(8,'jcon','2017-01-11 13:33:59','工作','d','d'),(9,'jcon','2017-01-11 00:00:00','工作','d','d888'),(11,'jcon','2017-01-11 13:34:00','工作','d','d'),(12,'jcon','2017-01-11 13:34:00','工作','d','d'),(13,'jcon','2017-01-11 00:00:00','工作','d','d'),(14,'jcon','2017-01-11 00:00:00','工作','d','d'),(16,'jcon','2017-01-11 13:34:00','工作','d','d'),(17,'jcon','2017-01-11 13:34:00','工作','d','d555'),(19,'jcon','2017-01-11 13:37:33','生活','dd','ddd'),(20,'jcon','2017-01-11 13:38:06','工作','gg','gg'),(21,'jcon','2017-01-11 13:38:17','工作','jhh','hh'),(24,'jcon','2017-01-18 20:52:57','工作','5','1'),(25,'jcon','2017-01-23 21:15:05','çæ´»','test','test'),(26,'jcon','2017-01-24 14:04:34','çæ´»','test','test');

/*Table structure for table `sociality_activity` */

DROP TABLE IF EXISTS `sociality_activity`;

CREATE TABLE `sociality_activity` (
  `ID` int(11) NOT NULL COMMENT '主键ID',
  `CREATE_TIME` datetime DEFAULT NULL COMMENT '创建时间',
  `ACCOUNT` varchar(20) DEFAULT NULL COMMENT '创建用户',
  `ACTIVITY_TIME` datetime DEFAULT NULL COMMENT '活动时间',
  `THEME` varchar(200) DEFAULT NULL COMMENT '活动主题',
  `TEAMMATE` varchar(200) DEFAULT NULL COMMENT '活动好友',
  `DURATION` varchar(20) DEFAULT NULL COMMENT '活动时长',
  `ADDRESS` varchar(200) DEFAULT NULL COMMENT '活动地方',
  `INCREASE` varchar(800) DEFAULT NULL COMMENT '活动获得的增长',
  `REMARKS` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sociality_activity` */

/*Table structure for table `sociality_contact` */

DROP TABLE IF EXISTS `sociality_contact`;

CREATE TABLE `sociality_contact` (
  `ID` int(11) NOT NULL COMMENT '主键ID',
  `CREATE_TIME` datetime DEFAULT NULL COMMENT '创建时间',
  `ACCOUNT` varchar(20) DEFAULT NULL COMMENT '创建用户',
  `CONTACT_TIME` datetime DEFAULT NULL COMMENT '联系时间',
  `CONTACTS` varchar(200) DEFAULT NULL COMMENT '联系人',
  `RELATION` varchar(20) DEFAULT NULL COMMENT '关系',
  `CONTACT_WAY` varchar(20) DEFAULT NULL COMMENT '联系方式',
  `CONTACT_LENGTH` varchar(20) DEFAULT NULL COMMENT '联系时长',
  `CONTACT_CONTENT` varchar(800) DEFAULT NULL COMMENT '联系内容',
  `IS_IMPORTANT` varchar(20) DEFAULT NULL COMMENT '是否重要',
  `REMARKS` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sociality_contact` */

/*Table structure for table `study_note` */

DROP TABLE IF EXISTS `study_note`;

CREATE TABLE `study_note` (
  `ID` int(11) NOT NULL COMMENT '主键ID',
  `CREATE_TIME` datetime DEFAULT NULL COMMENT '创建时间',
  `ACCOUNT` varchar(20) NOT NULL COMMENT '创建用户',
  `CONTENT` varchar(800) DEFAULT NULL COMMENT '内容',
  `SUMMARY` varchar(800) DEFAULT NULL COMMENT '归纳总结',
  `REMARKS` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `study_note` */

insert  into `study_note`(`ID`,`CREATE_TIME`,`ACCOUNT`,`CONTENT`,`SUMMARY`,`REMARKS`) values (2,'2017-02-01 09:26:26','jcon','测试','测试','测试5656');

/*Table structure for table `study_plan` */

DROP TABLE IF EXISTS `study_plan`;

CREATE TABLE `study_plan` (
  `ID` int(11) NOT NULL COMMENT '主键ID',
  `CREATE_TIME` datetime DEFAULT NULL COMMENT '创建时间',
  `ACCOUNT` varchar(20) NOT NULL COMMENT '创建用户',
  `PLAN_TYPE` varchar(20) DEFAULT NULL COMMENT '计划类型：长期、中期、短期',
  `CONTENT` varchar(800) DEFAULT NULL COMMENT '计划内容',
  `ACHIEVE_DESCRIBE` varchar(800) DEFAULT NULL COMMENT '实现详细',
  `PLAN_TIME` datetime DEFAULT NULL COMMENT '计划时间',
  `ACHIEVE_TIME` datetime DEFAULT NULL COMMENT '实际时间',
  `IS_ACHIEVE` varchar(20) DEFAULT NULL COMMENT '是否达成',
  `CAUSE` varchar(500) DEFAULT NULL COMMENT '原因',
  `REMARKS` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `study_plan` */

insert  into `study_plan`(`ID`,`CREATE_TIME`,`ACCOUNT`,`PLAN_TYPE`,`CONTENT`,`ACHIEVE_DESCRIBE`,`PLAN_TIME`,`ACHIEVE_TIME`,`IS_ACHIEVE`,`CAUSE`,`REMARKS`) values (1,'2017-02-09 21:23:43','jcon','短期','英语学习：每天学习5个单词','利用作公交的时间，通过墨墨记单词软件每天记5个单词，每周复习一次当周所记的单词，周末跟踪当周计划是否完成','2017-02-09 21:23:26','2017-02-09 21:23:32','','',''),(2,'2017-02-09 21:32:41','jcon','短期','java设计模式学习，共32种设计模式，计划一个月完成','按照菜鸟教程网站上的教程学习，每天学习1种设计模式，了解运用的场景及代码实现过程','2017-02-09 21:32:31','2017-02-09 21:32:35','','','');

/*Table structure for table `study_question` */

DROP TABLE IF EXISTS `study_question`;

CREATE TABLE `study_question` (
  `ID` int(11) NOT NULL COMMENT '主键ID',
  `CREATE_TIME` datetime DEFAULT NULL COMMENT '创建时间',
  `ACCOUNT` varchar(20) NOT NULL COMMENT '创建用户',
  `QUESTION` varchar(800) DEFAULT NULL COMMENT '问题描述',
  `SOLVE_METHOD` varchar(800) DEFAULT NULL COMMENT '解决办法',
  `SUMMARY` varchar(500) DEFAULT NULL COMMENT '总结',
  `REMARKS` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `study_question` */

insert  into `study_question`(`ID`,`CREATE_TIME`,`ACCOUNT`,`QUESTION`,`SOLVE_METHOD`,`SUMMARY`,`REMARKS`) values (2,'2017-02-01 09:37:17','jcon','测试','测试','测试','testddddddddd');

/*Table structure for table `study_task` */

DROP TABLE IF EXISTS `study_task`;

CREATE TABLE `study_task` (
  `ID` int(11) NOT NULL COMMENT '主键ID',
  `ACCOUNT` varchar(20) NOT NULL COMMENT '创建用户',
  `CREATE_TIME` datetime DEFAULT NULL COMMENT '创建时间',
  `TASK_DESCRIBE` varchar(800) DEFAULT NULL COMMENT '任务描述',
  `TASK_QUANTITY` varchar(500) DEFAULT NULL COMMENT '每天任务量',
  `PLAN_TIME` datetime DEFAULT NULL COMMENT '计划时间',
  `ACHIEVE_TIME` datetime DEFAULT NULL COMMENT '实际时间',
  `IS_ACHIEVE` varchar(20) DEFAULT NULL COMMENT '是否完成',
  `CAUSE` varchar(500) DEFAULT NULL COMMENT '原因',
  `REMARKS` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `study_task` */

insert  into `study_task`(`ID`,`ACCOUNT`,`CREATE_TIME`,`TASK_DESCRIBE`,`TASK_QUANTITY`,`PLAN_TIME`,`ACHIEVE_TIME`,`IS_ACHIEVE`,`CAUSE`,`REMARKS`) values (2,'jcon','2017-01-31 08:18:27','test','test','2017-01-31 08:18:12','2017-01-31 08:18:18','yes','good','very good'),(3,'jcon','2017-02-26 23:09:56','设计模式学习','每天学习一个设计模式，掌握原理及应用场景','2017-02-26 23:09:46',NULL,'','','');

/*Table structure for table `work_note` */

DROP TABLE IF EXISTS `work_note`;

CREATE TABLE `work_note` (
  `ID` int(11) NOT NULL COMMENT '主键id',
  `CREATE_TIME` datetime DEFAULT NULL COMMENT '创建时间',
  `ACCOUNT` varchar(20) NOT NULL COMMENT '创建用户',
  `CONTENT` varchar(800) DEFAULT NULL COMMENT '内容',
  `QUESTION` varchar(800) DEFAULT NULL COMMENT '问题点',
  `NOT_ENOUGH` varchar(800) DEFAULT NULL COMMENT '不足',
  `SUMMARIZE` varchar(800) DEFAULT NULL COMMENT '总结',
  `REMARKS` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `work_note` */

insert  into `work_note`(`ID`,`CREATE_TIME`,`ACCOUNT`,`CONTENT`,`QUESTION`,`NOT_ENOUGH`,`SUMMARIZE`,`REMARKS`) values (1,'2017-01-02 17:57:30','jcon','测试测试','好好学习','不够认真','不够坚持','好好加油'),(2,'2017-01-02 17:58:56','jcon','测试测试','好好学习','不够认真','不够坚持','好好加油'),(3,'2017-01-02 17:58:56','jcon','测试测试','好好学习','不够认真','不够坚持','好好加油'),(4,'2017-01-02 17:58:56','jcon','测试测试','好好学习','不够认真','不够坚持','好好加油'),(5,'2017-01-02 17:58:56','jcon','测试测试','好好学习','不够认真','不够坚持','好好加油'),(6,'2017-01-02 17:58:56','jcon','测试测试','好好学习','不够认真','不够坚持','好好加油'),(7,'2017-01-02 17:58:56','jcon','测试测试','好好学习','不够认真','不够坚持','好好加油'),(8,'2017-01-02 17:58:56','jcon','测试测试','好好学习','不够认真','不够坚持','好好加油'),(9,'2017-01-02 17:58:56','jcon','测试测试','好好学习','不够认真','不够坚持','好好加油'),(10,'2017-01-02 17:58:56','jcon','测试测试','好好学习','不够认真','不够坚持','好好加油'),(11,'2017-01-02 17:58:56','jcon','测试测试','好好学习','不够认真','不够坚持','好好加油'),(12,'2017-01-02 17:58:56','jcon','测试测试','好好学习','不够认真','不够坚持','好好加油'),(13,'2017-01-02 17:58:56','jcon','测试测试','好好学习','不够认真','不够坚持','好好加油'),(14,'2017-01-02 17:58:56','jcon','测试测试','好好学习','不够认真','不够坚持','好好加油'),(15,'2017-01-02 17:58:56','jcon','测试测试','好好学习','不够认真','不够坚持','好好加油'),(16,'2017-01-02 17:58:56','jcon','测试测试','好好学习','不够认真','不够坚持','好好加油'),(17,'2017-01-02 17:58:56','jcon','测试测试','好好学习','不够认真','不够坚持','好好加油'),(18,'2017-01-02 17:58:56','jcon','测试测试','好好学习','不够认真','不够坚持','好好加油'),(19,'2017-01-02 17:58:56','jcon','测试测试','好好学习','不够认真','不够坚持','好好加油'),(20,'2017-01-02 17:58:56','jcon','测试测试','好好学习','不够认真','不够坚持','好好加油'),(21,'2017-01-02 17:58:56','jcon','测试测试','好好学习','不够认真','不够坚持','好好加油'),(22,'2017-01-02 17:58:56','jcon','测试测试','好好学习','不够认真','不够坚持','好好加油'),(23,'2017-01-02 17:58:56','jcon','测试测试','好好学习','不够认真','不够坚持','好好加油'),(24,'2017-01-02 17:58:56','jcon','测试测试','好好学习','不够认真','不够坚持','好好加油'),(25,'2017-01-02 17:58:56','jcon','测试测试','好好学习','不够认真','不够坚持','好好加油'),(26,'2017-01-02 17:58:56','jcon','测试测试','好好学习','不够认真','不够坚持','好好加油'),(27,'2017-01-02 17:58:56','jcon','测试测试','好好学习','不够认真','不够坚持','好好加油'),(28,'2017-01-02 17:58:56','jcon','测试测试','好好学习','不够认真','不够坚持','好好加油'),(29,'2017-01-02 17:58:56','jcon','测试测试','好好学习','不够认真','不够坚持','好好加油'),(30,'2017-01-02 17:58:56','jcon','测试测试','好好学习','不够认真','不够坚持','好好加油'),(31,'2017-01-02 17:58:56','jcon','测试测试','好好学习','不够认真','不够坚持','好好加油'),(32,'2017-01-02 17:58:56','jcon','测试测试','好好学习','不够认真','不够坚持','好好加油'),(33,'2017-01-02 18:18:57','jcon','测试测试','好好学习','不够认真','不够坚持','好好加油'),(34,'2017-01-02 18:18:58','jcon','测试测试','好好学习','不够认真','不够坚持','好好加油'),(35,'2017-01-02 18:18:58','jcon','测试测试','好好学习','不够认真','不够坚持','好好加油'),(36,'2017-01-02 18:18:58','jcon','测试测试','好好学习','不够认真','不够坚持','好好加油'),(37,'2017-01-02 18:18:58','jcon','测试测试','好好学习','不够认真','不够坚持','好好加油'),(38,'2017-01-02 18:18:58','jcon','测试测试','好好学习','不够认真','不够坚持','好好加油'),(39,'2017-01-02 18:18:58','jcon','测试测试','好好学习','不够认真','不够坚持','好好加油'),(40,'2017-01-02 18:18:58','jcon','测试测试','好好学习','不够认真','不够坚持','好好加油'),(41,'2017-01-02 18:18:58','jcon','测试测试','好好学习','不够认真','不够坚持','好好加油'),(42,'2017-01-02 18:18:58','jcon','测试测试','好好学习','不够认真','不够坚持','好好加油'),(43,'2017-01-02 18:18:58','jcon','测试测试','好好学习','不够认真','不够坚持','好好加油'),(44,'2017-01-02 18:18:58','jcon','测试测试','好好学习','不够认真','不够坚持','好好加油'),(45,'2017-01-02 18:18:58','jcon','测试测试','好好学习','不够认真','不够坚持','好好加油'),(46,'2017-01-02 18:18:58','jcon','测试测试','好好学习','不够认真','不够坚持','好好加油'),(47,'2017-01-02 18:18:58','jcon','测试测试','好好学习','不够认真','不够坚持','好好加油'),(48,'2017-01-02 18:18:58','jcon','测试测试','好好学习','不够认真','不够坚持','好好加油'),(49,'2017-01-02 18:18:58','jcon','测试测试','好好学习','不够认真','不够坚持','好好加油'),(51,'2017-01-02 18:18:58','jcon','测试测试','好好学习','不够认真','不够坚持','好好加油'),(53,'2017-01-02 18:18:58','jcon','测试测试','好好学习','不够认真','不够坚持','好好加油'),(54,'2017-01-02 18:18:58','jcon','测试测试','好好学习','不够认真','不够坚持','好好加油'),(55,'2017-01-02 18:18:58','jcon','测试测试','好好学习','不够认真','不够坚持','好好加油'),(57,'2017-01-02 18:18:58','jcon','测试测试','好好学习','不够认真','不够坚持','好好加油'),(58,'2017-01-02 18:18:58','jcon','测试测试','好好学习','不够认真','不够坚持','好好加油'),(59,'2017-01-02 18:18:58','jcon','测试测试','好好学习','不够认真','不够坚持','好好加油'),(60,'2017-01-14 14:01:17','jcon','1','1','555555','1','1');

/*Table structure for table `work_plan` */

DROP TABLE IF EXISTS `work_plan`;

CREATE TABLE `work_plan` (
  `ID` int(11) NOT NULL COMMENT '主键',
  `CREATE_TIME` datetime DEFAULT NULL COMMENT '创建时间',
  `ACCOUNT` varchar(20) NOT NULL COMMENT '账户',
  `PLAN_TYPE` varchar(20) DEFAULT NULL COMMENT '计划类型(中期、长期)',
  `PLAN_DESCRIBE` varchar(800) DEFAULT NULL COMMENT '计划描述',
  `ACHIEVE_DESCRIBE` varchar(800) DEFAULT NULL COMMENT '达成描述(HOW)',
  `PLAN_TIME` datetime DEFAULT NULL COMMENT '计划时间',
  `ACHIEVE_TIME` datetime DEFAULT NULL COMMENT '实际完成时间',
  `IS_ACHIEVE` varchar(20) DEFAULT NULL COMMENT '是否完成',
  `SUMMARY` varchar(800) DEFAULT NULL COMMENT '总结',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `work_plan` */

insert  into `work_plan`(`ID`,`CREATE_TIME`,`ACCOUNT`,`PLAN_TYPE`,`PLAN_DESCRIBE`,`ACHIEVE_DESCRIBE`,`PLAN_TIME`,`ACHIEVE_TIME`,`IS_ACHIEVE`,`SUMMARY`) values (5,'2017-01-13 00:00:00','jcon','d','d','d','2017-01-05 00:00:00','2017-01-13 00:00:00','dd','66666666666666666');

/*Table structure for table `work_task` */

DROP TABLE IF EXISTS `work_task`;

CREATE TABLE `work_task` (
  `ID` int(11) NOT NULL COMMENT '主键ID',
  `CREATE_TIME` datetime DEFAULT NULL COMMENT '创建时间',
  `ACCOUNT` varchar(20) NOT NULL COMMENT '账户',
  `TASK_DESCRIBE` varchar(800) DEFAULT NULL COMMENT '任务描述',
  `TASK_DEMAND` varchar(800) DEFAULT NULL COMMENT '任务需求',
  `DEMAND_TIME` datetime DEFAULT NULL COMMENT '要求实际',
  `ACHIEVE_TIME` datetime DEFAULT NULL COMMENT '实际时间',
  `QUESTION` varchar(800) DEFAULT NULL COMMENT '遇到问题',
  `SUMMARY` varchar(800) DEFAULT NULL COMMENT '总结',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `work_task` */

insert  into `work_task`(`ID`,`CREATE_TIME`,`ACCOUNT`,`TASK_DESCRIBE`,`TASK_DEMAND`,`DEMAND_TIME`,`ACHIEVE_TIME`,`QUESTION`,`SUMMARY`) values (1,'2017-01-14 23:27:44','jcon','test','test','2017-01-14 23:27:26','2017-01-14 23:27:29','test','test88888'),(3,'2017-01-18 22:40:35','jcon','春节抢红包活动','1.彩乐乐项目和姿彩项目pc端和3g端均参与，活动时间为2017-02-14 09:30-23:59。\r\n2.彩乐乐红包大小为固定1.68元，参与资格为已注册并充值过的用户，总发红包个数不超2016个。\r\n3.姿彩红包为随机红包，8元80%,18元15%,68元3%,88元2%，参与资格为绑定了身份证、手机号及银行卡的用户，所发红包总金额不超13000元','2017-01-20 22:40:10','2017-01-19 22:40:15','1.action类中通过ajax传参数到jsp页面\r\n2.弹窗登录，登录后隐藏弹窗并刷新页面\r\n3.随机红包的算法','js不够熟悉，有待学习提升');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
