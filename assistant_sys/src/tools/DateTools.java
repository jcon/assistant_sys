package tools;


import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

public class DateTools {

	public final static String[] week = { "", "����", "��һ", "�ܶ�", "����", "����", "����", "����" };
	
	public final static String[] week2 = { "", "������", "����һ", "���ڶ�", "������", "������", "������", "������" };

	public static Date StringToDate(String dateString, String format) {
		Date date;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(format);
			date = sdf.parse(dateString);
		} catch (Exception e) {
			date = null;
			LogUtil.out(e);
		}

		return date;
	}

	public static String StringToDate_YYYY_MM_DD(Date date) {
		String dates = "";
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			dates = sdf.format(date);
		} catch (Exception e) {
			date = null;
			LogUtil.out(e);
		}

		return dates;

	}
	
	public static String StringToDate_MM_DD(Date date) {
		String dates = "";
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("MM-dd");
			dates = sdf.format(date);
		} catch (Exception e) {
			date = null;
			LogUtil.out(e);
		}

		return dates;

	}

	public static String StringToDate_MM_DD_HH_MM(Date date) {
		String dates = "";
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("MM-dd HH:mm");
			dates = sdf.format(date);
		} catch (Exception e) {
			date = null;
			LogUtil.out(e);
		}

		return dates;
	}
	
	public static Date StringToDate(String dateString) {
		Date date;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			date = sdf.parse(dateString);
		} catch (Exception e) {
			date = null;
			LogUtil.out(e);
		}

		return date;
	}
	
	public static Date StringtoDateHHMM(String dateString){
		Date date;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm");
			date = sdf.parse(dateString);
		} catch (Exception e) {
			date = null;
			LogUtil.out(e);
		}

		return date;
	}

	public static Date StringToDatetpy(String dateString) {
		Date date;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
			date = sdf.parse(dateString);
			System.out.println(date);
		} catch (Exception e) {
			date = null;
			LogUtil.out(e);
		}

		return date;
	}
	public static Date StringToDate_YYYY_MM_DD_HH_MM(String dateString) {
		Date date;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			date = sdf.parse(dateString);
		} catch (Exception e) {
			date = null;
			LogUtil.out(e);
		}

		return date;
	}

	public static Date StringToDate_YYYYMMDDHHMM(String dateString) {
		Date date;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
			date = sdf.parse(dateString);
		} catch (Exception e) {
			date = null;
			LogUtil.out(e);
		}

		return date;
	}

	public static Date StringToDate_YY_MM_DD_HH_MM(String dateString) {
		Date date;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yy-MM-dd HH:mm");
			date = sdf.parse(dateString);
		} catch (Exception e) {
			date = null;
			LogUtil.out(e);
		}

		return date;
	}

	public static Date StringToDate_YYYY_MM_DD_HH_MM_SS(String dateString) {
		Date date;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			date = sdf.parse(dateString);
		} catch (Exception e) {
			date = null;
			LogUtil.out(e);
		}

		return date;
	}

	public static Date stringToDate(String dateString, String format) {
		Date date;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(format);
			date = sdf.parse(dateString);
		} catch (Exception e) {
			date = null;
			LogUtil.out(e);
		}
		return date;
	}

	public static String dateToString(Date date, String format) {
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.format(date);
	}

	public static Date StringToDateShort(String dateString) {
		Date date;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			date = sdf.parse(dateString);
		} catch (Exception e) {
			date = null;
			LogUtil.out(e);
		}

		return date;
	}

	public static String dateToString(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return sdf.format(date);
	}
	
	public static String dateToStringYYYY_MM_DD_HH_MM(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm");
		return sdf.format(date);
	}

	public static String getByTimeMillis(long timeMillis) {
		Calendar c = Calendar.getInstance();
		c.setTimeInMillis(timeMillis);
		return dateToString(c.getTime());

	}

	public static long getTimeMillis(String dateTime) {
		if (dateTime == null || dateTime.equals("")) {
			return 0l;
		}
		Date date = DateTools.StringToDate(dateTime);
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		return c.getTimeInMillis();
	}

	public static String getNowDateYYYYMMDD() {
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		return sdf.format(date);
	}

	public static String getNowDate(String format) {
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.format(date);
	}

	public static String getNowDateYYMMDD() {
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyMMdd");
		return sdf.format(date);
	}
	
	public static String getNowDateYYYYMM(){
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMM");
		return sdf.format(date);
	}

	public static String getNowDateYYYY_MM_DD() {
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		return sdf.format(date);
	}

	public static String getNowDateYYYY_MM_DD_HH_MM() {
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		return sdf.format(date);
	}

	public static String getNowDateYYYY_MM_DD_HH_MM_SS() {
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return sdf.format(date);
	}

	public static String getNowDateYYYYMMDDHHMMSS() {
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		return sdf.format(date);
	}

	public static String getNowDateYYYYMMDDHHMMSSSS() {
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSS");
		return sdf.format(date);
	}
	
	public static String getDateYYYYMMDDHHMMSS(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		return sdf.format(date);
	}

	public static Integer handleIntTime(Date date, String weekDayName) {
		for (int i = 0; i <= 3; i++) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(date);
			cal.add(Calendar.DAY_OF_MONTH, -i);
			int day = cal.get(Calendar.DAY_OF_WEEK);
			String tempWeek = week[day];
			if (tempWeek.equals(weekDayName))
				return Integer.valueOf(dateToString(cal.getTime(), "yyyyMMdd"));
		}
		return null;
	}
	
	public static String getWeekStr2(Date date,String[] week) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		return week[cal.get(Calendar.DAY_OF_WEEK)];
	}

	public static String getWeekStr(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		return week[cal.get(Calendar.DAY_OF_WEEK)];
	}
	
	public static int getWeekInt(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		return cal.get(Calendar.DAY_OF_WEEK);
	}
	
	/**
	 * ������� 
	 * �ų�1�·ݻ�ȡͬ��12�·�ʱ��&12�·ݻ�ȡͬ��1�·�ʱ��
	 * @param dateStr
	 * @param date �ο�ʱ��
	 * @return
	 */
	public static Date strToDateDafueYear(String dateStr,Date date) {
		Date nowDate=new Date();
		String nowYear=DateTools.dateToString(nowDate,"yyyy");
		Date matchDate = DateTools.StringToDate(nowYear+"-"+dateStr, "yyyy-MM-dd HH:mm");
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(matchDate);
		Calendar now = Calendar.getInstance();
		//�вο�ֵʹ�òο�ֵ�����
		if(date!=null){
			now.setTime(date);
			calendar.set(Calendar.YEAR, now.get(Calendar.YEAR));
		}else{
			//�ų�1�·ݻ�ȡͬ��12�·�ʱ��&12�·ݻ�ȡͬ��1�·�ʱ��
			if(calendar.getTimeInMillis()<now.getTimeInMillis()&&calendar.get(Calendar.MONTH)==0&&now.get(Calendar.MONTH)==11){
				calendar.set(Calendar.YEAR, now.get(Calendar.YEAR)+1);
			}else if(calendar.getTimeInMillis()>now.getTimeInMillis()&&calendar.get(Calendar.MONTH)==11&&now.get(Calendar.MONTH)==0){
				calendar.set(Calendar.YEAR, now.get(Calendar.YEAR)-1);
			}
		}
		return calendar.getTime();
	}
	public static Date strToDateDafueYear(String dateStr) {
		return strToDateDafueYear(dateStr,null);
	}
	public static Date getDateByOffsetDay(Date date, int offset) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DAY_OF_YEAR, offset);
		return cal.getTime();
	}
	
	public static Date getDateByOffsetTime(Date date, int offset) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.MINUTE, offset);
		return cal.getTime();
	}

	public static boolean isDateBefore(String date2, Date date1) {
		try {
			DateFormat df = DateFormat.getDateTimeInstance();
			return date1.before(df.parse(date2));
		} catch (ParseException e) {
			System.out.print(e.getMessage());
			return false;
		}
	}
	
	public static boolean isIndexDCDateBefore(Date date){
		try {
			Calendar c = Calendar.getInstance();
			GregorianCalendar ca = new GregorianCalendar(); 
			
			if(ca.get(GregorianCalendar.AM_PM) == 1)//�ж�������ʱ��
				c.set(Calendar.HOUR, -2);
			else
				c.set(Calendar.HOUR, +10);
			
	        c.set(Calendar.SECOND, 0);
	        c.set(Calendar.MINUTE, 0);

			Date afterTime=c.getTime();//����10ʱ��
			return afterTime.before(date);
		} catch (Exception e) {
			System.out.print(e.getMessage());
			return false;
		}
	}
	
	public static boolean isIndexDCDateAfter(Date date){
		try {
			Calendar c = Calendar.getInstance();
			GregorianCalendar ca = new GregorianCalendar(); 
			
			if(ca.get(GregorianCalendar.AM_PM) == 1)//�ж�������ʱ��
				c.set(Calendar.HOUR, +22);
			else
				c.set(Calendar.HOUR, +34);
			
	        c.set(Calendar.SECOND, 0);
	        c.set(Calendar.MINUTE, 0);

			Date beforeTime=c.getTime();//����10ʱǰ
			return beforeTime.after(date);
		} catch (Exception e) {
			System.out.print(e.getMessage());
			return false;
		}
		
	}

	public static Date GetDCMatchEndTime(Date matchTime, int aheadMilli) {
		if(matchTime == null) return null;
		Calendar matchCal = Calendar.getInstance();
		matchCal.setTime(matchTime);
		
		Calendar startDate=(Calendar) matchCal.clone();
		startDate.set(Calendar.HOUR_OF_DAY, 6);
		startDate.set(Calendar.MINUTE, 0);
		startDate.set(Calendar.SECOND, 0);
		
		Calendar endDate=(Calendar) matchCal.clone();
		endDate.set(Calendar.HOUR_OF_DAY, 9);
		endDate.set(Calendar.MINUTE, 31);
		endDate.set(Calendar.SECOND, 0);
		
		//�ȽϿ���ʱ��.����� 6:00-9:31 ֮��ı���. ����ǰ��6:00 - aheadMilli ��ֹ
		if (matchCal.after(startDate) && matchCal.before(endDate)) {
			startDate.add(Calendar.MILLISECOND, -aheadMilli);
			return startDate.getTime();
		}else{
			matchCal.add(Calendar.MILLISECOND, -aheadMilli);
			return matchCal.getTime();
		} 
		
	}
	
	
	/**
     * ��ȡ�������ڵ�ʱ��
     */
    public static String getMinutes (Date times){
        long time = new Date().getTime() - times.getTime();//time ��λ�� ����
        String res = null;
        //ת��������
        //���ж��ǲ���С�� 60 * 60 * 1000  Ҳ���� С��1Сʱ����ô��ʾ �� **����ǰ
        if(time < 60 * 60 * 1000){
            res =  (time / 1000 / 60 ) + "����ǰ";
        }
        //�����ڵ���1Сʱ С�ڵ���һ�죬��ô��ʾ �� **Сʱǰ
        else if(time >= 60 * 60 * 1000 && time < 24 * 60 * 60 * 1000){
            res =  (time / 1000 / 60 / 60 ) + "Сʱǰ";
        }
        //�����ڵ���1Сʱ С�ڵ���һ�죬��ô��ʾ �� **Сʱǰ
        else if(time >= 24 * 60 * 60 * 1000){
            res =  (time / 1000 / 60 / 60 /24 ) + "��ǰ";
        }
        //���ʱ�䲻��ȷ���߷�����һ���� ������ʾ
        else{
            res = "";
        }
        
        return res;
    }
    
    /**
     * ȡ��ϵͳ��ǰʱ��ǰn���µ����Ӧ��һ��

     * @param n int
     * @return String yyyy-mm-dd
     */
    public static String getNMonthBeforeCurrentDay(int n) {
      Calendar c = Calendar.getInstance();
      c.add(Calendar.MONTH, -n);
      return dateToString(c.getTime(), "yyyy-MM-dd");

    }
    
    public static List<String> getASCDatesBetween(String startDateTxt, String endDateTxt,String format) {
    	List<String> dates = null;
		dates = new ArrayList<String>();
		Date startDate = StringToDate(startDateTxt,format);
		Date endDate = StringToDate(endDateTxt,format);
		Calendar calendarTemp = Calendar.getInstance();
		calendarTemp.setTime(startDate);
		while (!calendarTemp.getTime().equals(endDate)) {
			dates.add(dateToString(calendarTemp.getTime(), format));
			calendarTemp.add(Calendar.DAY_OF_YEAR, 1);
		}
		dates.add(endDateTxt);
		return dates;
    }
    
    public static List<String> getDESCDatesBetween(String startDateTxt, String endDateTxt,String format) {
    	List<String> dates = null;
    	try {
    		dates = new ArrayList<String>();
    		Date startDate = new SimpleDateFormat(format).parse(startDateTxt);
    		Date endDate = new SimpleDateFormat(format).parse(endDateTxt);
    		Calendar calendarTemp = Calendar.getInstance();
    		calendarTemp.setTime(endDate);
    		
    		while (!calendarTemp.getTime().equals(startDate)) {
    			dates.add(dateToString(calendarTemp.getTime(), format));
    			calendarTemp.add(Calendar.DAY_OF_YEAR, -1);
    		}
    		dates.add(startDateTxt);
    	} catch (ParseException e) {
    			e.printStackTrace();
    	}
		return dates;
    }
    public static List<String> getDESCDatesBetween(String startDateTxt, String endDateTxt,String format,Date minDate) {
    	List<String> dates = null;
    	try {
    		dates = new ArrayList<String>();
    		Date startDate = new SimpleDateFormat(format).parse(startDateTxt);
    		Date endDate = new SimpleDateFormat(format).parse(endDateTxt);
    		Calendar calendarTemp = Calendar.getInstance();
    		calendarTemp.setTime(endDate);
    		
    		while (!calendarTemp.getTime().equals(startDate)) {
    			if(minDate!=null && calendarTemp.getTime().before(minDate)){
    				return dates;
    			}
    			dates.add(dateToString(calendarTemp.getTime(), format));
    			calendarTemp.add(Calendar.DAY_OF_YEAR, -1);
    		}
    		dates.add(startDateTxt);
    	} catch (ParseException e) {
    			e.printStackTrace();
    	}
		return dates;
    }
    /*******�õ���������*******/
    public static String getYesterDay(){
    	Calendar cal = Calendar.getInstance(); 
	    cal.add(Calendar.DATE,-1); 
	    return new SimpleDateFormat("yyyyMMdd").format(cal.getTime());
    }
	
    /**
     * <p>�õ���������</p>
     * @param pattern String
     * @return String
     */
    public static String getYesterDay(String pattern) {
    	Calendar cal = Calendar.getInstance();
	    cal.add(Calendar.DATE, -1);
	    return new SimpleDateFormat(pattern).format(cal.getTime());
    }
    
	/**
	 * ��ȡ��������֮��������
	 * @param startDate
	 * @param endDate
	 * @return
	 */
    public static String getTwoDay(Date startDate, Date endDate) {
		long day = 0;
		try {
			day = (endDate.getTime() - startDate.getTime()) / (24 * 60 * 60 * 1000);
		} catch (Exception e) {
			return "";
		}
		return day+"";
	}
    /**
     * ��ȡ��������֮��������
     * @param startDate
     * @param endDate
     * @return
     */
    public static Long getTwoTime(Date startDate,Date endDate){
    	long ss=0;
    	try {
    		ss=(endDate.getTime() - startDate.getTime()) / 1000;
		} catch (Exception e) {
			return 0L;
		}
		return ss;
    	
    }
    
    /**
     * �ж��Ƿ�����������֮��
     * @param startDate
     * @param endDate
     * @return
     */
    public static boolean isCompareDate(Date startDate,Date endDate){
    	Date today=new Date();
    	if(startDate.before(today) && endDate.after(today)){
    		return true;
    	}
		return false;
    }
    
    /**
     * <p>��ȡ���쿪ʼʱ��</p>
     * @return Date
     */
    public static Date getStartDateOnCurrDay() {
    	Calendar cal = Calendar.getInstance();
    	cal.set(Calendar.HOUR_OF_DAY, 0);
    	cal.set(Calendar.MINUTE, 0);
    	cal.set(Calendar.SECOND, 0);
    	return cal.getTime();
    }
    
    /**
     * <p>��ȡ�������ʱ��</p>
     * @return Date
     */
    public static Date getEndDateOnCurrDay() {
    	Calendar cal = Calendar.getInstance();
    	cal.set(Calendar.HOUR_OF_DAY, 23);
    	cal.set(Calendar.MINUTE, 59);
    	cal.set(Calendar.SECOND, 59);
    	return cal.getTime();
    }
    
    /**
     * <p>��ȡ���ܿ�ʼʱ��</p>
     * @return Date
     */
    public static Date getStartDateOnCurrWeek() {
    	Calendar cal = Calendar.getInstance();
    	cal.setFirstDayOfWeek(Calendar.MONDAY);
    	cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
    	cal.set(Calendar.HOUR_OF_DAY, 0);
    	cal.set(Calendar.MINUTE, 0);
    	cal.set(Calendar.SECOND, 0);
    	return cal.getTime();
    }
    
    /**
     * <p>��ȡ���ܽ���ʱ��</p>
     * @return Date
     */
    public static Date getEndDateOnCurrWeek() {
    	Calendar cal = Calendar.getInstance();
    	cal.setFirstDayOfWeek(Calendar.MONDAY);
    	cal.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
    	cal.set(Calendar.HOUR_OF_DAY, 23);
    	cal.set(Calendar.MINUTE, 59);
    	cal.set(Calendar.SECOND, 59);
    	return cal.getTime();
    }
    
    /**
     * <p>��ȡ���¿�ʼʱ��</p>
     * @return Date
     */
    public static Date getStartDateOnCurrMonth() {
    	Calendar cal = Calendar.getInstance();
    	cal.set(Calendar.DAY_OF_MONTH, 1);
    	cal.set(Calendar.HOUR_OF_DAY, 0);
    	cal.set(Calendar.MINUTE, 0);
    	cal.set(Calendar.SECOND, 0);
    	return cal.getTime();
    }
    
    /**
     * <p>��ȡ���½���ʱ��</p>
     * @return Date
     */
    public static Date getEndDateOnCurrMonth() {
    	Calendar cal = Calendar.getInstance();
    	cal.set(Calendar.DAY_OF_MONTH, 1);
    	cal.add(Calendar.MONTH, 1);
    	cal.add(Calendar.DAY_OF_MONTH, -1);
    	cal.set(Calendar.HOUR_OF_DAY, 23);
    	cal.set(Calendar.MINUTE, 59);
    	cal.set(Calendar.SECOND, 59);
    	return cal.getTime();
    }
    
	
	public static boolean isSameDate(Date date1, Date date2) {
	       Calendar cal1 = Calendar.getInstance();
	       cal1.setTime(date1);
	       Calendar cal2 = Calendar.getInstance();
	       cal2.setTime(date2);
	       boolean isSameYear = cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR);
	       boolean isSameMonth = isSameYear && cal1.get(Calendar.MONTH) == cal2.get(Calendar.MONTH);
	       boolean isSameDate = isSameMonth && cal1.get(Calendar.DAY_OF_MONTH) == cal2.get(Calendar.DAY_OF_MONTH);

	       return isSameDate;
	   }
	
	/**
	 * ���ص�ǰ�����ڼ����������ڣ�1��2��3��4��5��6��7)
	 * @return
	 */
	public static int getNowDayInWeekdays(){
		Calendar now = Calendar.getInstance();
		// һ�ܵ�һ���Ƿ�Ϊ������
		boolean isFirstSunday = (now.getFirstDayOfWeek() == Calendar.SUNDAY);
		int weekDay = now.get(Calendar.DAY_OF_WEEK);
		// ��һ�ܵ�һ��Ϊ�����죬��-1
		if (isFirstSunday) {
			weekDay = weekDay - 1;
			if (weekDay == 0) {
				weekDay = 7;
			}
		}
		return weekDay;
	}
    
	public static void main(String args[]) {
		Date matchTime=DateTools.StringToDate("2013-12-19 05:00:00");
		System.out.println(GetDCMatchEndTime(matchTime, 15*60*1000).toLocaleString());
		
		matchTime=DateTools.StringToDate("2013-12-19 06:00:00");
		System.out.println(GetDCMatchEndTime(matchTime, 15*60*1000).toLocaleString());
		
		matchTime=DateTools.StringToDate("2013-12-19 06:10:00");
		System.out.println(GetDCMatchEndTime(matchTime, 15*60*1000).toLocaleString());
		
		matchTime=DateTools.StringToDate("2013-12-19 09:30:00");
		System.out.println(GetDCMatchEndTime(matchTime, 15*60*1000).toLocaleString());
		
		matchTime=DateTools.StringToDate("2013-12-19 09:31:00");
		System.out.println(GetDCMatchEndTime(matchTime, 15*60*1000).toLocaleString());
	}
	
	public static  String getWeekOfDate(Date dt) {
        String[] weekDays = {"������", "����һ", "���ڶ�", "������", "������", "������", "������"};
        Calendar cal = Calendar.getInstance();
        cal.setTime(dt);

        int w = cal.get(Calendar.DAY_OF_WEEK) - 1;
        if (w < 0)
            w = 0;

        return weekDays[w];
    }
}
