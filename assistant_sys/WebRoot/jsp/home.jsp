<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<!-- 引入 jQuery 核心库 -->
<script type="text/javascript" src="/easyui/jquery.min.js"></script>
<!-- 引入 jQuery EasyUI 核心库，这里采用的是 1.3.6 -->
<script type="text/javascript" src="/easyui/jquery.easyui.min.js"></script>
<!-- 引入 EasyUI 中文提示信息 -->
<script type="text/javascript" src="/easyui/locale/easyui-lang-zh_CN.js"></script>
<!-- 引入 EasyUI 核心 UI 文件 CSS-->

<link rel="stylesheet" type="text/css"
	href="/easyui/themes/default/easyui.css" />
<!--引入 EasyUI 图标文件 -->
<link rel="stylesheet" type="text/css" href="/easyui/themes/icon.css" />
<link rel="stylesheet" type="text/css" href="/style/index2.css">
<title>首页</title>

<style type="text/css">
.menuItems {
	width: 140px;
	height: 25px;
	padding-top: 5px;
	margin: 5px;
	clear: none;
	float: left;
	text-align: center;
	clear: none;
}

.MainGuidCss {
	width: 100px;
	height: 25px;
	padding-top: 75px;
	margin: 20px;
	clear: none;
	float: left;
	text-align: center;
	clear: none;
}

.menuItems:hover {
	background-color: #6699CC;
}
</style>
<script type="text/javascript">
	$(document)
			.ready(
					function() {
						var menuList = {
							/* 联系人 */
							'>>联系人' : '/contacts/contacts_index',
						
							/* 学习菜单 */
							'>>学习计划' : '/study/plan_index',
							'>>学习任务' : '/study/task_index',
							'>>遇到问题' : '/study/question_index',
							'>>学习笔记' : '/study/note_index',
							
							/* 工作相关 */
							'>>工作目标' : '/work/plan_index',
							'>>工作任务' : '/work/task_index',
							'>>工作日记' : '/work/note_index',
							'>>备忘录' : '/common/memorandum_index?type=工作',

							/* 生活娱乐 */
							'>>娱乐活动' : '/life/disport_index',
							'>>生活备忘录' : '/common/memorandum_index?type=生活',

							/* 健康锻炼 */
							'>>身体概况' : '/health/summarize_index',
							'>>运动记录' : '/health/sports_index',
							'>>体检历史' : '/health/check_index',
							'>>个人病历' : '/health/caseHistory_index',
							
							/* 社交活动 */
							'>>社交活动' : '/sociality/activity_index',
							'>>联系记录' : '/sociality/contact_index',
							'>>个人备忘录' : '/common/memorandum_index?type=社交',

							/* 家庭相关 */
							'>>家人联系' : '/family/contact_index',
							'>>家庭活动' : '/family/activity_index',
							'>>个人备忘录' : '/common/memorandum_index?type=家庭',

							/* 财务收支 */
							'>>待开发' : '',

							/* 系统设置 */
							'>>备份数据' : '',
							'>>恢复数据' : '',
							'>>系统结存' : '',
							'>>系统重建' : '',

							'>>系统配置' : '',
							'>>权限设置' : '',
							'>>修改密码' : '',
							'>>更改操作员' : '',

							'>>联系我们' : '',
							'>>关于我们' : ''
						};

						// 在这里写你的代码...
						$("#menuItem > div").addClass("menuItems");

						function mybind() {
							$("#menuItem > div")
									.bind(
											"click",
											function() {
												var menuText = $(this).text();
												var exist_tab = $('#frame_tabs')
														.tabs('getTab',
																menuText);
												//判断是否已经打开该选项卡 
												if (exist_tab) {
													$('#frame_tabs').tabs(
															'select', menuText);
													return;
												} else {

													var tabsrc = menuList[menuText];
													var contentCC = '<iframe   frameborder="0"  src="'
															+ tabsrc
															+ '" style="width:100%;height:100%"></iframe>';
													$('#frame_tabs')
															.tabs(
																	'add',
																	{
																		'id' : 'tab',
																		title : menuText,
																		fit : true,
																		content : contentCC,
																		closable : true
																	});
												}
											});
						}
				;
						mybind();
						$("#frame_tabs").tabs(
								{
									onSelect : function(title, index) {
										if (index == 0) {
											var $selectAcc = $(
													".easyui-accordion")
													.accordion('getSelected');
											/*  alert($P.html()); */
											$("#MainGuid").empty();
											$("#MainGuid").append(
													$selectAcc.html());
											mybind();
											$("#MainGuid > #menuItem > div")
													.addClass("MainGuidCss");
										}
									}
								});

					}

			);
</script>
</head>
<body class="easyui-layout">
	<div data-options="region:'west',title:'菜单',split:true" style="width:220px;">
		<div class="easyui-accordion" data-options="fit:true" style="width:220px;">
			<div title="☆联系人" style="overflow:auto">
				<!-- 菜单0 -->
				<div id="menuItem">
					<div>>>联系人</div>
				</div>
			</div>
			<div title="☆学习菜单" style="overflow:auto">
				<!-- 菜单1 -->
				<div id="menuItem">
					<div>>>学习计划</div>
					<div>>>学习任务</div>
					<div>>>遇到问题</div>
					<div>>>学习笔记</div>
				</div>
			</div>
			<div title="☆工作相关" style="overflow:auto">
				<!-- 菜单2 -->
				<div id="menuItem">
					<div>>>工作目标</div>
					<div>>>工作任务</div>
					<div>>>工作日记</div>
					<div>>>备忘录</div>
				</div>
			</div>

			<div title="☆生活娱乐" style="overflow:auto">
				<!-- 菜单3 -->
				<div id="menuItem">
					<div>>>娱乐活动</div>
					<div>>>生活备忘录</div>
				</div>
			</div>
			
			<div title="☆健康锻炼" style="overflow:auto">
				<!-- 菜单4 -->
				<div id="menuItem">
					<div>>>身体概况</div>
					<div>>>运动记录</div>
					<div>>>体检历史</div>
					<div>>>个人病历</div>
				</div>
			</div>
			
			<div title="☆社交活动" style="overflow:auto">
				<!-- 菜单5 -->
				<div id="menuItem">
					<div>>>社交活动</div>
					<div>>>联系记录</div>
					<div>>>个人备忘录</div>
				</div>
			</div>

			<div title="☆家庭相关" style="overflow:auto">
				<!-- 菜单7 -->
				<div id="menuItem">
					<div>>>家人联系</div>
					<div>>>家庭活动</div>
					<div>>>个人备忘录</div>
				</div>
			</div>
			
			<div title="☆财务收支" style="overflow:auto">
				<!-- 菜单8 -->
				<div id="menuItem">
					<div>>>待开发</div>
				</div>
			</div>
			
			<div title="☆系统设置" style="overflow:auto">
				<!-- 菜单9 -->

				<div id="menuItem">
					<div>>>备份数据</div>
					<div>>>恢复数据</div>
					<div>>>系统结存</div>
					<div>>>系统重建</div>
					<div>>>系统配置</div>
					<div>>>权限设置</div>
					<div>>>修改密码</div>
					<div>>>更改操作员</div>
					<div>>>联系我们</div>
					<div>>>关于我们</div>
				</div>
			</div>
		</div>
	</div>

	<!-- 首页头部 -->
	<div data-options="region:'north',border:false"
		style="height:60px;background:#B3DFDA;padding:2px;margin:0px">
		<h2 style="margin:10px">&nbsp;&nbsp;&nbsp;个人助理系统</h2>
		<div align="right" style="padding:0px;margin:0px"><span>当前用户：${ sessionScope.user} |</span>
		<a href="/sys/sys_loginOut">退出系统</a>
		</div>
	</div>

	<!-- 首页底部 -->
	<div data-options="region:'south',border:false"
		style="height:20px;background:#A9FACD;padding:3px">
		<div align="center"><span>仅供个人学习用 @by-Jcon</span></div>
	</div>
	
	<!-- 中间列表部分 -->
	<div data-options="region:'center'">
		<div class="easyui-tabs" data-options="fit:true" id="frame_tabs">

			<!--二级tab  -->
			<div title="（导     航 ）" id="MainTab"
				data-options="closable:false ,fit:true" style="overflow:auto;">
				<div id="MainGuid"></div>
			</div>
		</div>
	</div>

</body>
</html>
