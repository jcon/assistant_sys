<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<!-- 引入 jQuery 核心库 -->
<script type="text/javascript" src="/easyui/jquery.min.js"></script>
<!-- 引入 jQuery EasyUI 核心库，这里采用的是 1.3.6 -->
<script type="text/javascript" src="/easyui/jquery.easyui.min.js"></script>
<!-- 引入 EasyUI 中文提示信息 -->
<script type="text/javascript" src="/easyui/locale/easyui-lang-zh_CN.js"></script>
<!-- 引入时间格式转换相关的js -->
<script type="text/javascript" src="/js/date.js"></script>

<!-- 引入 EasyUI 核心 UI 文件 CSS-->

<link rel="stylesheet" type="text/css"
	href="/easyui/themes/default/easyui.css" />
<!--引入 EasyUI 图标文件 -->
<link rel="stylesheet" type="text/css" href="/easyui/themes/icon.css" />
<style type="text/css">
</style>
<script type="text/javascript">
	/*tree初始化 */
	$(document).ready(function() {
		$('#dlg').dialog('close');
		$('#tt').tree({
			lines : true,
			url : 'classifyAction_list.action',
		});

	});

	function deleteit() {
		var node = $('#tt').tree('getSelected');
		/*取得URL*/

		var path = $("#form1").attr("action").substring(0,
				$("#form1").attr("action").indexOf("_"))
				+ "_deleteById?id=" + node.id;
		alert(path);
		$.post(path, function(data) {
			$('#tt').tree();
		});
	}

	/*已完成，缺少条件判断，应该用检测是否重名  */
	function renameit() {
		var node = $('#tt').tree('getSelected');
		if (node) {
			/*alert("renameit()" + node.id); */

			var parentA = $('#tt').tree('getParent', node.target);
			if (parentA == null) {
				alert("请选择所有分类下的一个商品节点");
				return;
			}
			;
			$('#dlg').dialog('open');
			$('#cc').combotree('setValue', parentA.id);
			$("#treeTextid").val(node.id);
			$("#treeText").val(node.text);
		}

	}

	function addtoit() {
		var node = $('#tt').tree('getSelected');
		if (node) {
			$('#dlg').dialog('open');
			$('#cc').combotree('setValue', node.id);
			$("#treeTextid").val(null);
			$("#treeText").val(null);
		}
	}

	function getValue() {
		var val = $('#cc').combotree('getValue');
		alert(val);
	}

	$(function() {
		$("#treeRename").bind("click", function() {
			var path = $("#form1").attr("action");

			$.post(path, $("#form1").serialize(), function(data) {
				$('#dlg').dialog('close');
				$('#tt').tree();
				$('#cc').combotree();
			});

		});
	});

	var toolbar = [
			{
				text : ' 导入',
				iconCls : 'icon-add',
				handler : function(text) {
					alert(text);
				}
			},
			{
				text : '导出',
				iconCls : 'icon-cut',
				handler : function() {
					alert('cut');
				},
			},
			'-',
			{
				text : '上移',
				iconCls : 'icon-save',
				handler : function() {
					alert('save');
				},
			},
			{
				text : '下移 ',
				iconCls : 'icon-save',
				handler : function() {
					alert('save');
				},
			},
			{
				text : '保存排序',
				iconCls : 'icon-save',
				handler : function() {
					alert('save');
				},
			},
			{
				text : '新增',
				iconCls : 'icon-add',
				handler : function() {
					$('#win').window('open');
					$(":reset").click();
					$('#win').window('maximize');
					$("#form2 :button").val("提交新增");
					/* $(":input[name='id']").val(null); */

				},
			},
			{
				text : '编辑',
				iconCls : 'icon-add',
				handler : function() {
					$('#win').window('open');
					$('#win').window('maximize');
					$("#form2 :button").val("提交编辑");
					var selected = $('#dg').datagrid('getSelected');
					if (selected) {
						$.each(selected, function(i, n) {
							$(":input[name=" + i + "]").val(n);
						});
					}

				},
			},
			{
				text : '删除',
				iconCls : 'icon-remove',
				handler : function() {
					alert('确认删除吗？');
					var selected = $('#dg').datagrid('getSelected');
					if (selected) {
						$.each(selected, function(i, n) {
							$(":input[name=" + i + "]").val(n);
							if (i == "id") {
								var path = $("#form2").attr("action")
										.substring(
												0,
												$("#form2").attr("action")
														.indexOf("_"))
										+ "_deleteById?id=" + n;
								$.get(path, function(data) {
									$('#dg').datagrid("reload", {});
								});
							}
						});
					}
				},
			}, {
				text : '打印',
				iconCls : 'icon-print',
				handler : function() {
					alert('save');
				},
			}, ];

	function haha() {
		$(".fieldTable :text").each(function() {

			console.info($(this).val());
		});
	}
	/* 以js方式加载datagrid */
	$(function() {
		$('#dg').datagrid({
			rownumbers : true,
			singleSelect : true,
			pagination : true,
			toolbar : toolbar,
			fit : true,
			autoRowHeight : false,
			striped : true,
			url : 'contact_getJsonList',
			columns: [[  
                     { title: '联系时间', field: 'contactTime',formatter: formatDatebox, editor: 'datebox', sortable: true, width:160,align:'center'},  
                     { title: '联系人', field: 'contacts', sortable: true, width:200,align:'center'},  
                     { title: '联系方式', field: 'contactWay', sortable: true, width:190,align:'center'},  
                     { title: '时长', field: 'contactLength', sortable: true, width:190,align:'center'},  
                     { title: '内容', field: 'content', sortable: true, width:190,align:'center'},  
                     { title: '备注', field: 'remarks', sortable: true, width:160,align:'center'},  
                 ]],

			onHeaderContextMenu : function(e, field) {
				e.preventDefault();
				if (!cmenu) {
					createColumnMenu();
				}
				cmenu.menu('show', {
					left : e.pageX,
					top : e.pageY
				});
			}
		});
		/* 	表头菜单，可以让用户选择显示那些 列 */ 
		var cmenu;
		function createColumnMenu() {
			cmenu = $('<div/>').appendTo('body');
			cmenu.menu({
				onClick : function(item) {
					if (item.iconCls == 'icon-ok') {
						$('#dg').datagrid('hideColumn', item.name);
						cmenu.menu('setIcon', {
							target : item.target,
							iconCls : 'icon-empty'
						});
					} else {
						$('#dg').datagrid('showColumn', item.name);
						cmenu.menu('setIcon', {
							target : item.target,
							iconCls : 'icon-ok'
						});
					}
				}
			});
			var fields = $('#dg').datagrid('getColumnFields');
			for (var i = 0; i < fields.length; i++) {
				var field = fields[i];
				var col = $('#dg').datagrid('getColumnOption', field);
				cmenu.menu('appendItem', {
					text : col.title,
					name : field,
					iconCls : 'icon-ok'
				});
			}
		}
		$('#win').window('close');
	});

	/*给提交button绑定事件  */
	$(function() {
		$("#datagridSubmit")
				.bind(
						"click",
						function() {
							var path;
							if ($("#form2 :button").val() == "提交编辑") {
								path = "_update";
							} else {
								path = "_save";
							}
							path = $("#form2").attr("action").substring(0,
									$("#form2").attr("action").indexOf("_"))
									+ path;
							$
									.post(
											path,
											$("#form2").serializeArray(),
											function(data) {
												$('#dg').datagrid("reload", {});

												if ($("#form2 :button").val() == "提交编辑") {
													alert('修改成功！');
													$(":reset").click();
													$('#win').window('close');
												} else {
													
														alert('增加成功！');
														
														$(":input[name]").val(
																null);
														$('#win').window(
																'close');
													
												}
											});

						});

	});
	/*
	 * $(function() { $(":button").bind( "click", function() {
	 * alert($("form").attr("action")+"?"+$("form").serialize());
	 * $.get($("form").attr("action") + "?" + $("form").serialize(),
	 * function(data) {
	 * 
	 * $('#dg').datagrid("reload", {
	 * 
	 * }); });
	 * 
	 * });
	 * 
	 * });
	 */
</script>
<style type="text/css">
.fieldTable {
	border-style: none;
	border-spacing: 5px;
}

.fieldTable tr td {
	font-size: 12px;
	padding: 3px;
	text-align: right;
}

.fieldTable tr td input[type=textfield] {
	border-style: none;
	border-bottom-style: solid;
	border-bottom-width: 1px;
	border-bottom-color: black;
	background-color: white;
}
*{margin:0px;padding:0px;}
body{background-color:#f9f9f9;}

.clears{ clear:both;}
/*messages*/
.messages{padding:15px 0;}
.messages input,.messages select,.messages textarea{margin:0;padding:0; background:none; border:0; font-family:"Microsoft Yahei";}
.messlist {height:30px;margin-bottom:10px;}
.messlist label{float:left;width:100px; height:30px; font-size:14px; line-height:30px; text-align:right;padding-right:10px;}
.messlist input{float:left;width:300px;height:28px;padding-left:5px;border:#ccc 1px solid;}
.messlist.textareas{ height:auto;}
.messlist textarea{float:left;width:400px; height:60px;padding:5px;border:#ccc 1px solid;}
.messlist.yzms input{width:100px;}
.messlist.yzms .yzmimg{ float:left;margin-left:10px;}
.messsub{padding:0px 0 0 110px;}
.messsub input{width:100px; height:35px; background:#ddd; font-size:14px; font-weight:bold; cursor:pointer;margin-right:5px}
.messsub input:hover{ background:#f60;color:#fff;}
#label0{display:none;color:#0aa770;height:28px;line-height:28px;}
#label1{display:none;color:#0aa770;height:28px;line-height:28px;}
#label2{display:none;color:#0aa770;height:28px;line-height:28px;}
#label3{display:none;color:#0aa770;height:28px;line-height:28px;}
#label4{display:none;color:#0aa770;height:28px;line-height:28px;}
#label5{display:none;color:#0aa770;height:28px;line-height:28px;}
#label6{display:none;color:#0aa770;height:28px;line-height:28px;}
#label7{display:none;color:#0aa770;height:28px;line-height:28px;}
#label8{display:none;color:#0aa770;height:48px;line-height:48px;}
#label9{display:none;color:#0aa770;height:48px;line-height:48px;}
#label10{display:none;color:#0aa770;height:48px;line-height:48px;}
</style>
</head>
<body class="easyui-layout">
	<!-- class="easyui-datagrid" 同时使用和class和js将导致两次加载问题 -->
	<div data-options="region:'center'">
		<table id="dg">
		</table>
		<div></div>
		<div id="win" class="easyui-window" title="编辑工作目标"
			style="width:800px;height:400px;padding:5px;">
			<s:form action="/family/contact_" id="form2">
			<table class="fieldTable">
				<s:hidden name="id"></s:hidden>
				<s:hidden name="account"></s:hidden>
				<s:hidden name="createTime"></s:hidden>
				<div class="messlist textareas">
					<label>联系时间：</label>
					<input class="easyui-datetimebox" name="contactTime" data-options="required:true,showSeconds:true" >
					<div id="label8">√</div>
					<div id="label9">X 字数超过了800字</div>
					<div id="label10">* 不超800字</div>
					<div class="clears"></div>
				</div>
				<div class="messlist">
					<label>联系人：</label> <input type="text" placeholder="联系了谁呢..." id="input1" name="contacts"
						onblur="jieshou()">
					<div id="label0">*你还没填写名字呢!</div>
					<div id="label1">√正确</div>
					<div id="label2">×错误</div>
					<div class="clears"></div>
				</div>
				<div class="messlist">
					<label>联系方式：</label> <input type="text" placeholder="通过电话还是QQ联系的呢..." id="input1" name="contactWay"
						onblur="jieshou()">
					<div id="label0">*你还没填写名字呢!</div>
					<div id="label1">√正确</div>
					<div id="label2">×错误</div>
					<div class="clears"></div>
				</div>
				<div class="messlist">
					<label>联系时长：</label> <input type="text" placeholder="聊了多久呢..." id="input1" name="contactLength"
						onblur="jieshou()">
					<div id="label0">*你还没填写名字呢!</div>
					<div id="label1">√正确</div>
					<div id="label2">×错误</div>
					<div class="clears"></div>
				</div>
				<div class="messlist textareas">
					<label>内容：</label>
					<textarea placeholder="都聊了什么..." id="input4" name="content" onblur="content()"></textarea>
					<div id="label8">√</div>
					<div id="label9">X 字数超过了800字</div>
					<div id="label10">* 不超800字</div>
					<div class="clears"></div>
				</div>
				<div class="messlist">
					<label>备注：</label> <input type="text" placeholder="总结一下..." id="input1" name="remarks"
						onblur="jieshou()">
					<div id="label0">*你还没填写名字呢!</div>
					<div id="label1">√正确</div>
					<div id="label2">×错误</div>
					<div class="clears"></div>
				</div>


				<div class="messsub">
					<input id="datagridSubmit" type="button" value="提交新增" onclick="getDate()"
						style="background:#00a3eb;color:#fff;"/> <input type="reset"
						value="清空" />
				</div>
				</table>
			</s:form>
		</div>
	</div>
<!-- 表单js代码部分begin -->
<script>
function send(){

var name = document.getElementById("input1").value;
var mail = document.getElementById("input2").value;
var phone = document.getElementById("input3").value;
var content = document.getElementById("input4").value;
var code = document.getElementById("input5").value;
if(name=="")
{
label2.style.display = 'none';
label1.style.display = 'none';
label0.style.display = 'block';;
return false;
}
if(mail=="")
{
label3.style.display = 'none';
label4.style.display = 'none';
label5.style.display = 'block';;
return false;
}

if(content=="")
{
        label8.style.display = 'none'; 
        label9.style.display = 'none'; 
        label10.style.display = 'block'; //*必填
        return false;
}
if(code=="")
{
   alert('请填写验证码！');
   return false;  
}
else{
alert('信息已发送到站长邮箱，感谢您的支持！');
}

 

}
</script>
<script>
function jieshou(){
var label1 = document.getElementById("label1");
var label2 = document.getElementById("label2");
var nametext = document.getElementById("input1").value;
if(nametext!=""){
label0.style.display = 'none';
label1.style.display = 'block';
label2.style.display = 'none';
}
else{
label0.style.display = 'block';
label1.style.display = 'none';
label2.style.display = 'none';
}
 
}
</script>

<script>
function mailtext(){
var mailvalue = document.getElementById("input2").value;
var mailtext = document.getElementById("input2");
var myreg = /^([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+@([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+\.[a-zA-Z]{2,3}$/;
if(mailvalue!=""){ //邮箱如果非空 显示正确
label5.style.display = 'none';  
label3.style.display = 'block';//显示正确
label4.style.display = 'none';       
}
else{
label5.style.display = 'block';//显示*必填
label3.style.display = 'none';
label4.style.display = 'none'; 
return false;
}
//上面为一段


if(!myreg.test(mailvalue)){  //既而 正则表达式 验证邮箱 如果不是邮箱地址label4显示出来
        label3.style.display = 'none'; 
        label4.style.display = 'block'; //*邮箱地址错误
        return false;
    }    
else{
        label3.style.display = 'block'; 
        label5.style.display = 'none';
        label4.style.display = 'none';
}
//上面为一段

}
</script>

<script>
function phonetext(){
var phonetext = document.getElementById("input3").value;
if(!(/^1[3|4|5|7|8]\d{9}$/.test(phonetext))){ 
        label6.style.display = 'none'; 
        label7.style.display = 'block'; //*手机号码错误
        return false;
    } 
else{
        label6.style.display = 'block'; 
        label7.style.display = 'none';
}
}

</script>

</script>

<script>
function content(){
var content = document.getElementById("input4").value;
if(content!=""){ 
        label8.style.display = 'block'; 
        label9.style.display = 'none'; 
        label10.style.display = 'none'; //*必填
        return false;
    } 
else{
        label8.style.display = 'none'; 
        label9.style.display = 'none'; 
        label10.style.display = 'block';  
}

}

</script>

<!-- 表单js部分end -->
</body>
</html>
