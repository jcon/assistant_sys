#检出步骤   
1.把该项目的git地址复制，在Eclipse或者MyEclipse中导入。  
2.建立项目所需的数据库，执行项目中src目录下的assistant_sys.sql语句。   
3.修改连接数据的账号跟密码，在config\db.properties改为你本地连接数据库的对应账号和密码。 
4.把项目部署到Tomcat然后运行，登录页面为login.html，账号:jcon 密码:admin。    

#搭建环境   
1.项目框架是Struts+Spring+Hibernate,用Maven对jar包进行管理，页面显示使用EasyUI,数据库使用的是MySQL。   
2.数据传输使用json进行传输。   
	
#项目截图
![image](http://git.oschina.net/jcon/assistant_sys/raw/master/assistant_sys/WebRoot/images/login.png)	
![image](http://git.oschina.net/jcon/assistant_sys/raw/master/assistant_sys/WebRoot/images/index.png)